package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FilterEmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cout = scanner.nextInt();

        List<EmpoyeeID> employees = new ArrayList<>();
        for (int i = 0; i <cout ; i++) {
            EmpoyeeID employee = EmpoyeeById.readEmployee(scanner);
            employees.add(employee);
        }

        employees.removeIf(FilterEmpoyeeById::DNIwhitA);
        employees.forEach(System.out::println);
    }

    private static boolean DNIwhitA(EmpoyeeID empoyeeID) {
    char DNIwA = empoyeeID.getDni().charAt(empoyeeID.getDni().length()-1);
    return DNIwA=='A';
    }
}
