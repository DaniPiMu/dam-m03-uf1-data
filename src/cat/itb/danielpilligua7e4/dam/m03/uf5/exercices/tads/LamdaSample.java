package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class LamdaSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);

        //eliminamos los que acaban en 3
        values.removeIf(integer -> integer%10==3);

        // ordenamos de mayor a menos
        values.sort((v1,v2) -> v2-v1);

        //los imprimimos por pantalla
        values.forEach(integer -> System.out.println(integer));
    }

}
