package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

public class Country {
    String name;
    String capital;
    int area;
    int density;

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public int getArea() {
        return area;
    }

    public int getDensity() {
        return density;
    }

    public Country(String name, String capital, int area, int density) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.density = density;

    }
}