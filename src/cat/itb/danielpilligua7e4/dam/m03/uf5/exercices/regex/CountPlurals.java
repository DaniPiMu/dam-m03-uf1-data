package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountPlurals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();
        String regex= "\\w+s[ ,\\. ]";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher= pattern.matcher(userInput);

        int contador = 0;
        while (matcher.find()){
            contador++;
        }
        System.out.println(contador);
    }
}
