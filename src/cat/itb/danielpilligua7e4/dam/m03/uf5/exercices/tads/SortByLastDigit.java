package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortByLastDigit {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(253,432,65,234,43,16,28,432,34,65,312,34,2134,76,2,76,23,67,27,8,54235256,4560,7431);
        list.sort(Comparator.comparing(SortByLastDigit::getLastDigit));
        list.forEach(System.out::println);
    }

    private static Integer getLastDigit(Integer i) {
        return i%10;
    }
}
