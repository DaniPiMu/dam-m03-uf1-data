package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RepeatedAnswer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<String> scategories = new HashSet<>();
        String fruit = scanner.next(); // es diu user? que conté
        while (!fruit.equals("END")) {
            boolean value = scategories.contains(fruit);
            if (value == true) {
                System.out.println("MEEEEC");
            } else {
                scategories.add(fruit);
            }
            fruit = scanner.next();

        }
    }
}
