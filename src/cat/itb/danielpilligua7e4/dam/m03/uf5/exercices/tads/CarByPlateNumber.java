package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CarByPlateNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String,Car> listaCoches = new HashMap<>();
        int userInput = scanner.nextInt();
        for (int i = 0; i <userInput ; i++) {
            Car car = readCar(scanner);
            listaCoches.put(car.getMatricula(),car);

        }
        String matricula= scanner.nextLine();
        while (!matricula.equals("END")){
            Car car =listaCoches.get(matricula);
            System.out.println(car);
            matricula = scanner.nextLine();
        }
    }

    private static Car readCar(Scanner scanner) {
        String matricula = scanner.next();
        String model= scanner.next();
        String color= scanner.next();
        scanner.nextLine();
        return new Car(matricula,model,color);
    }
}
