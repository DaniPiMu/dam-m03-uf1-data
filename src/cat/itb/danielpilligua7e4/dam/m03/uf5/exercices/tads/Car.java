package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

public class Car {
    String matricula;
    String model;
    String color;

    public String getMatricula() {
        return matricula;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Car(String matricula, String model, String color) {
        this.matricula = matricula;
        this.model = model;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "matricula='" + matricula + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
