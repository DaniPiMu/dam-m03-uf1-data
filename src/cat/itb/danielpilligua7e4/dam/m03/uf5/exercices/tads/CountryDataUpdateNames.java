package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class CountryDataUpdateNames {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        List<Country> countryList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Country country = readCountry(scanner);
            countryList.add(country);
        }

        countryList.stream()
                .map(CountryDataUpdateNames::toUpperCase)
                .forEach(System.out::println);
    }

    private static Country toUpperCase(Country country) {
        String name = country.name.toUpperCase();
        String capital= country.capital.toUpperCase();
        return new Country(name, capital, country.area, country.density);
    }

    private static Country readCountry(Scanner scanner) {
        String name = scanner.next();
        String capital = scanner.next();
        int area = scanner.nextInt();
        int density = scanner.nextInt();
        return new Country(name, capital, area, density);

    }
}
