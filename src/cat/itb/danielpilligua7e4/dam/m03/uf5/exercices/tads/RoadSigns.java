package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RoadSigns {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    Map<Integer, String>registre = new HashMap<>();
    int max = scanner.nextInt();

    for (int i=0;i<max;i++){
        int metres = scanner.nextInt();
        String name = scanner.next();
        registre.put(metres,name);
    }
    int metresUser= scanner.nextInt();
    while (metresUser !=-1){
        String sign = registre.get(metresUser);
        if (sign ==null){
            System.out.println("no hi ha cartell");
        }else{
            System.out.println(sign);
        }
        metresUser= scanner.nextInt();
    }
    }
}
