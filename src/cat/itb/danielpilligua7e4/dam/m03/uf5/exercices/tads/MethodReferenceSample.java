package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MethodReferenceSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);
        values.removeIf(MethodReferenceSample::endsWithTree);
        values.sort(MethodReferenceSample::descendinOrder);
        values.forEach(MethodReferenceSample::print);
    }

    private static void print(Integer integer) {
        System.out.println(integer);
    }

    private static int descendinOrder(Integer integer1, Integer integer2) {

        return integer2-integer1;
    }

    private static boolean endsWithTree(Integer integer) {
        return integer%10==3;
    }
}
