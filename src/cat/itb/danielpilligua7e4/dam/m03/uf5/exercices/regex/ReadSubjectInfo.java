package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadSubjectInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        List<String> list = new ArrayList<>();
        String regex = "\\d+s[ ,\\.]";

        for (int i = 0; i < count; ++i) {
            String text = scanner.next();
            list.add(text);
        }

        for (int i = 0; i < list.size(); ++i) {
            String text = list.get(i);
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
        }
    }
}
