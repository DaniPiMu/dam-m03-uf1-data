package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cuantEmp = scanner.nextInt();

        Map<String,EmpoyeeID> employeeMap = new HashMap<>();
        for (int i =0;i<cuantEmp;i++){
            EmpoyeeID employee= readEmployee(scanner);
            employeeMap.put(employee.getDni(),employee);
        }
        String dni = scanner.nextLine();
        while(!dni.equals("END")){
            EmpoyeeID employee=employeeMap.get(dni);
            System.out.println(employee);
        }
    }
    public static EmpoyeeID readEmployee(Scanner scanner) {
        String dni = scanner.nextLine();
        String nom = scanner.nextLine();
        String cognoms = scanner.nextLine();
        String adreça = scanner.nextLine();
        return new EmpoyeeID(dni,nom,cognoms,adreça);
    }
}
