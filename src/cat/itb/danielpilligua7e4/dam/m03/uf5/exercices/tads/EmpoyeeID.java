package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

public class EmpoyeeID {
    String dni;
    String nom;
    String cognoms;
    String adreça;

    public String getDni() {
        return dni;
    }

    public String getNom() {
        return nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public String getAdreça() {
        return adreça;
    }

    public EmpoyeeID(String dni, String nom, String cognoms, String adreça) {
        this.dni = dni;
        this.nom = nom;
        this.cognoms = cognoms;
        this.adreça = adreça;
    }

    @Override
    public String toString() {
        return String.format("%s %s - %s, %s", nom,cognoms,dni,adreça);
    }
}
