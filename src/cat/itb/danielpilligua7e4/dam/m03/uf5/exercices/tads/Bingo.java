package cat.itb.danielpilligua7e4.dam.m03.uf5.exercices.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Bingo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> numeros = new HashSet<>();
        int tarjetas = 10;

        for (int i = 0; i < tarjetas; i++) {
            int userNumber = scanner.nextInt();
            numeros.add(userNumber);
        }

        while (!numeros.isEmpty()) {
            int number = scanner.nextInt();
            numeros.remove(number);
            System.out.println("se ha borrado el " + number);
            System.out.println("Qeudan " +numeros.size());
        }
        System.out.println("bingo");
    }
}