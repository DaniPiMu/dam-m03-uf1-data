package cat.itb.danielpilligua7e4.dam.m03.uf5.generalexam;

public class Book {
    String titol;
    String autor;
    String ISBN;
    int numPaginas;
    int años;

    public Book(String titol, String autor, String ISBN, int numPaginas, int años) {
        this.titol = titol;
        this.autor = autor;
        this.ISBN = ISBN;
        this.numPaginas = numPaginas;
        this.años = años;
    }

    public String getTitol() {
        return titol;
    }

    public String getAutor() {
        return autor;
    }

    public String getISBN() {
        return ISBN;
    }

    public int getNumPaginas() {
        return numPaginas;
    }

    public int getAños() {
        return años;
    }
}
