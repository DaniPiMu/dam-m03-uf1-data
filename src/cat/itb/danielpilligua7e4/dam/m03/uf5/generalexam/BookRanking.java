package cat.itb.danielpilligua7e4.dam.m03.uf5.generalexam;

import java.util.*;

//No he podido probar si el reslutado al ejecutar esta bien, pero he intentado hacer las formulas...

public class BookRanking {
    public static void main(String[] asrgs) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        List<Book> books = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Book book = SearchByISBN.readBook(scanner);
            books.add(book);
        }
        //el libro con mas paginas
        Collections.max(books,Comparator.comparing(Book::getNumPaginas));

        //El llistat dels diferents llibres ordenats per any de publicació (any més gran primer).
        // Els llibres del mateix any, ordenar-los per el títol.

        books.stream().sorted(Comparator.comparing(Book::getAños))
                .forEach(System.out::println);

        //media de paginas de lobros publicados
        double average = books.stream()
                .mapToInt(book -> book.getNumPaginas())
                .average().getAsDouble();
        System.out.println(average);

        //La informació dels llibres publicats després del 2018
        books.stream()
                .filter(book -> book.getAños()>2018)
                .forEach(System.out::println);

        //La suma del resultat de múltiplicar el nombre de pàgines per l'any de publicació de cada llibre
        books.stream().map(book -> book.getNumPaginas()*book.getAños()).forEach(System.out::println);

        //Dels llibres de més de 100 pàgines, ordenats per ordre alfabètic del títol, imprimir-ne el títol.
        books.stream().filter(book -> book.getNumPaginas()>100)
                .sorted(Comparator.comparing(Book::getTitol))
                .forEach(System.out::println);
    }



}
