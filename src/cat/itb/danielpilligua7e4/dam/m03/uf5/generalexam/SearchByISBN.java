package cat.itb.danielpilligua7e4.dam.m03.uf5.generalexam;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

// Cuando intento introducir el segudno libro, a la hora de meter el ISBN me peta... El error me dice que,
//parece ser que no he metido bien el dato"InputMismatchException"... Pero yo veo todo correctamente
//Y se me hace mas comodo que el programa acabe con un END por eso lo he puesto

public class SearchByISBN {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        Map<String,Book> editorial = new HashMap<>();
        int max = scanner.nextInt();
        for (int i = 0; i < max; i++) {
            Book book = readBook(scanner);
            editorial.put(book.getISBN(), book);
        }
        String ISBN = scanner.nextLine();
        while (!ISBN.equals("END")){
            Book book = editorial.get(ISBN);
            System.out.println(book);
            ISBN= scanner.nextLine();
        }
    }

    public static Book readBook(Scanner scanner) {
        String titol = scanner.nextLine();
        String autor= scanner.nextLine();
        String ISBN = scanner.nextLine();
        int numPaginas = scanner.nextInt();
        int años = scanner.nextInt();
        return new Book(titol,autor,ISBN,numPaginas,años);
    }
}
