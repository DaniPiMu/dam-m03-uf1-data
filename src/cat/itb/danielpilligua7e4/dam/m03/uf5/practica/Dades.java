package cat.itb.danielpilligua7e4.dam.m03.uf5.practica;

public class Dades {
    String nom;
    String codi;
    int casosNousConfirmats;
    int totalConfirmat;
    int novesMorts;
    int mortsTotals;
    int nousRecuperats;
    int recupertatsTotals;

    public Dades(String nom, String codi, int casosNousConfirmats, int totalConfirmat, int novesMorts, int mortsTotals, int nousRecuperats, int recupertatsTotals) {
        this.nom = nom;
        this.codi = codi;
        this.casosNousConfirmats = casosNousConfirmats;
        this.totalConfirmat = totalConfirmat;
        this.novesMorts = novesMorts;
        this.mortsTotals = mortsTotals;
        this.nousRecuperats = nousRecuperats;
        this.recupertatsTotals = recupertatsTotals;
    }

    public String getNom() {
        return nom;
    }

    public String getCodi() {
        return codi;
    }

    public int getCasosNousConfirmats() {
        return casosNousConfirmats;
    }

    public int getTotalConfirmat() {
        return totalConfirmat;
    }

    public int getNovesMorts() {
        return novesMorts;
    }

    public int getMortsTotals() {
        return mortsTotals;
    }

    public int getNousRecuperats() {
        return nousRecuperats;
    }

    public int getRecupertatsTotals() {
        return recupertatsTotals;
    }

    @Override
    public String toString() {
        return "Dades{" +
                "nom='" + nom + '\''+ '\n' +
                " codi='" + codi + '\'' + '\n' +
                " casosNousConfirmats=" + casosNousConfirmats + '\n' +
                " totalConfirmat=" + totalConfirmat + '\n' +
                " novesMorts=" + novesMorts + '\n' +
                " mortsTotals=" + mortsTotals + '\n' +
                " nousRecuperats=" + nousRecuperats + '\n' +
                " recupertatsTotals=" + recupertatsTotals + '\n' +
                '}';
    }
}
