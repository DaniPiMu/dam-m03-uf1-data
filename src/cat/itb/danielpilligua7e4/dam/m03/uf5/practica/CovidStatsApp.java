package cat.itb.danielpilligua7e4.dam.m03.uf5.practica;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class CovidStatsApp {

    public static void main(String[] args) throws IOException {
        //Dades
        Path covidFile = Paths.get("./src/cat/itb/danielpilligua7e4/dam/m03/uf5/practica/coviddata.txt");
        List<Dades> countries = new ArrayList<>();
        Scanner scanner = new Scanner(covidFile);
        while (scanner.hasNext()) {
            String nom = scanner.nextLine();
            String codi = scanner.nextLine();
            int casosNousConfirmats = scanner.nextInt();
            int totalConfirmat = scanner.nextInt();
            int novesMorts = scanner.nextInt();
            int mortsTotals = scanner.nextInt();
            int nousRecuperats = scanner.nextInt();
            int recupertatsTotals = scanner.nextInt();
            scanner.nextLine();
            countries.add(new Dades(nom, codi, casosNousConfirmats, totalConfirmat, novesMorts, mortsTotals, nousRecuperats, recupertatsTotals));
        }
        System.out.println(countries);
        printTotal(countries);
        printTopEU(countries);
        printTops(countries);



    }
    //totals
    private static void printTotal(List<Dades> countries) {
        System.out.printf("### DADES TOTALS ###\n" +
                "Casos nous: %s\n" +
                "Casos totals: %s\n", contadorCasosNuevos(countries), contadorCasosTotales(countries));
    }

    private static int contadorCasosTotales(List<Dades> countries) {
        int total = 0;
        for (Dades country : countries){
            int totalCases = country.getTotalConfirmat();
            total += totalCases;
        }
        return total;

    }

    private static int contadorCasosNuevos(List<Dades> countries) {
        int total = 0;
        for (Dades country : countries){
            int casosNuevos = country.getCasosNousConfirmats();
            total += casosNuevos;
        }
        return total;

    }

    //PrintTops
    private static void printTops(List<Dades> countries) {
        System.out.printf("\n### Tops ###\n" +
                "Païs amb més casos nous: %s\n" +
                "Païs amb més casos totals: %s\n", calcMasNuevosCasos(countries).getNom(), calcNuevoCasosTotales(countries).getNom());
    }

    private static Dades calcNuevoCasosTotales(List<Dades> countries) {
        return Collections.max(countries, Comparator.comparing(Dades::getTotalConfirmat));
    }

    private static Dades calcMasNuevosCasos(List<Dades> countries) {
        return Collections.max(countries, Comparator.comparing(Dades::getCasosNousConfirmats));
    }
    //TOP EU data
    private static void printTopEU(List<Dades>countries){
        countries.removeIf(CovidStatsApp::paisNoEuropeo);
        System.out.printf("\n### Tops EU ###\n" +
                "Païs amb més casos nous: %s\n" +
                "Païs amb més casos totals: %s\n", calcMasNuevosCasos(countries).getNom(), calcNuevoCasosTotales(countries).getNom());
    }

    private static boolean paisNoEuropeo(Dades dades) {
        String[] ids = {"BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL"};
        for (String id : ids){
            if (dades.getCodi().equals(id)){
                return false;
            }
        }
        return true;

    }
    //EU Tops By Population
    private static void printEUTopsPopulations(List<Dades>countries){
        countries.removeIf(CovidStatsApp::paisNoEuropeo);
        System.out.printf("\n### Tops EU ###\n" +
                "Païs amb més casos nous: %s\n" +
                "Païs amb més casos totals: %s\n", calcMasNuevosCasos(countries).getNom(), calcNuevoCasosTotales(countries).getNom());
    }

}