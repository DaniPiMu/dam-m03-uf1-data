package cat.itb.danielpilligua7e4.dam.m03.uf4.practica;

public class Knight extends ChessPiece{
    public Knight(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♞";
    }

    @Override
    public boolean movement(int column, int row, int moveToColumn, int moveToRow) {
        if ((column-moveToColumn)*(column-moveToColumn) +(row-moveToRow)*(row-moveToRow) == 5){
            return true;
        }else
            return false;
    }
}