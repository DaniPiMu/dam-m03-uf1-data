package cat.itb.danielpilligua7e4.dam.m03.uf4.practica;

public class Rock extends ChessPiece{
    public Rock(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♜";
    }

    @Override
    public boolean movement(int column, int row, int moveToColumn, int moveToRow) {

        if (column==moveToColumn || row==moveToRow){
            return true;
        }else {
            return false;
        }
    }
}
