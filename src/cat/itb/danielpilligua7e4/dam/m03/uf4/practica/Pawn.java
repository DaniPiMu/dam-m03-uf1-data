package cat.itb.danielpilligua7e4.dam.m03.uf4.practica;

public class Pawn extends ChessPiece {
    public Pawn(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♟";
    }

    @Override
    public boolean movement(int column, int row, int moveToColumn, int moveToRow) {
        if (white) {
            if (column - moveToColumn == 1 && row - moveToRow == 1) {
                return true;
            } else
                return false;
        } else {
            if (column - moveToColumn == -1 && row - moveToRow == -1) {
                return true;
            } else
                return false;
        }
    }
}

