package cat.itb.danielpilligua7e4.dam.m03.uf4.practica;

import java.util.Scanner;

public class ChessBoardApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ChessBoard chessBoard=new ChessBoard();
        chessBoard.paint();
        int userValue = scanner.nextInt();
        while (userValue!=-1){
            chessBoard.movement(userValue);
            chessBoard.paint();
            userValue= scanner.nextInt();
        }
    }
}
