package cat.itb.danielpilligua7e4.dam.m03.uf4.practica;

public class Bishop extends ChessPiece{
    public Bishop(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♝";
    }

    @Override
    public boolean movement(int column, int row, int moveToColumn, int moveToRow) {
        if (Math.abs(column-moveToColumn) == Math.abs(row-moveToRow)){
            return true;
        }else
            return false;
    }
}

