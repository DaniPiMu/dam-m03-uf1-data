package cat.itb.danielpilligua7e4.dam.m03.uf4.practica;

public class King extends ChessPiece{
    public King(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♚";
    }

    @Override
    public boolean movement(int column, int row, int moveToColumn, int moveToRow) {
        if (Math.abs(column-moveToColumn)<=1 && Math.abs(row-moveToRow)<=1){
            return true;
        }else
            return false;
    }
}

