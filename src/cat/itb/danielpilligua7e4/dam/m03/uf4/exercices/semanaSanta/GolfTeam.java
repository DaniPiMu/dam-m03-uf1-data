package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

public class GolfTeam extends Team {
    String GolfistaName;

    public GolfTeam(String name, String shout, String golfistaName) {
        super(name, shout);
        GolfistaName = golfistaName;
    }

    @Override
    public void shoutMotto() {
        System.out.println(getShout()+ " " +GolfistaName);
    }
}
