package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.figures;

import java.io.PrintStream;

public class RectangleFigure extends Figure {
    int width;
    int height;

    public RectangleFigure(String color, int width, int height) {
        super(color);
        this.width = width;
        this.height = height;
    }


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void paint(PrintStream printStream){
        printStream.print(color);
        for (int i = 0; i < height ; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print("X");
            }
            printStream.println();
        }
        printStream.print(ConsoleColors.RESET);
    }

    @Override
    protected void paintDots(PrintStream printStream) {

    }
}

