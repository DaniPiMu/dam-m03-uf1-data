package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices;

public class VehicleModel {
    String name;
    BycicleBrand brand;

    public VehicleModel(String name, BycicleBrand brand) {
        this.name = name;
        this.brand = brand;
    }
}
