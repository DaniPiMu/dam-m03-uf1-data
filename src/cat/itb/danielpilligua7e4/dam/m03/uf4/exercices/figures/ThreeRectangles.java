package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.figures;

import cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.figures.ConsoleColors;
import cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.figures.RectangleFigure;

public class ThreeRectangles {
    public static void main(String[] args) {

        RectangleFigure rectangleFigure = new RectangleFigure(ConsoleColors.RED, 4, 5);
        rectangleFigure.paint(System.out);
        RectangleFigure rectangleFigure1 = new RectangleFigure(ConsoleColors.YELLOW, 2, 2);
        rectangleFigure1.paint(System.out);
        RectangleFigure rectangleFigure2 = new RectangleFigure(ConsoleColors.GREEN, 3, 5);
        rectangleFigure2.paint(System.out);
    }
}
