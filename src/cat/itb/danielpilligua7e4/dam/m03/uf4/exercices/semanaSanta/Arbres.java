package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

import java.time.LocalDateTime;

public class Arbres extends EsserViu {
    boolean viu;

    public Arbres(String name, String data, boolean viu) {
        super(name, data);
        this.viu = viu;
    }

    @Override
    public String toString() {
        return "Arbres{" +
                "viu=" + viu +
                ", name='" + name + '\'' +
                ", data=" + data +
                '}';
    }
}
