package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.figures;

public class ThreeRectanglesOneLeftPiramid {
    public static void main(String[] args) {

        RectangleFigure rectangleFigure = new RectangleFigure(ConsoleColors.RED, 4, 5);
        rectangleFigure.paint(System.out);

        LeftPiramidFigure piramidFigure = new LeftPiramidFigure(ConsoleColors.YELLOW, 3);
        piramidFigure.paint(System.out);

        RectangleFigure rectangleFigure2 = new RectangleFigure(ConsoleColors.GREEN, 3, 5);
        rectangleFigure2.paint(System.out);
    }
}
