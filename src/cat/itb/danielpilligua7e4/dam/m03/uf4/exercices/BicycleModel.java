package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices;

public class BicycleModel extends VehicleModel{
    int gears;

    public BicycleModel(String name, BycicleBrand brand, int gears) {
        super(name, brand);
        this.gears = gears;
    }

    @Override
    public String toString() {
        return "BicycleModel{" +
                "name='" + name + '\'' +
                ", gears=" + gears +
                ", brand=" + brand +
                '}';
    }


}
