package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices;

public class Student {
    String name;
    Notes notes;

    public Student(String name, Notes notes) {
        this.name = name;
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", notes=" + notes +
                '}';
    }
}
