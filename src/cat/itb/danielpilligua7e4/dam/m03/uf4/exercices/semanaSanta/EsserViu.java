package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

import java.time.LocalDateTime;

public class EsserViu {
    String name;
    String data;

    public EsserViu(String name, String data) {
        this.name = name;
        this.data = data;
    }

    @Override
    public String toString() {
        return "EsserViu{" +
                "name='" + name + '\'' +
                ", data=" + data +
                '}';
    }
}
