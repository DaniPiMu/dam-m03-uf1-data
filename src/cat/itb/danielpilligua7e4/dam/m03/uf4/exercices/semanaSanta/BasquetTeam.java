package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

public class BasquetTeam extends Team {
    String newShout;

    public BasquetTeam(String name, String shout, String newShout) {
        super(name, shout);
        this.newShout = newShout;
    }

    @Override
    public void shoutMotto() {
        System.out.println(newShout+ " " + getShout());
    }
}
