package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices;

import java.util.ArrayList;
import java.util.List;

public class StudentWithTextGrade {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        Student student1 = new Student("Antonio", Notes.BE);
        list.add(student1);
        System.out.println(list);
    }
}
