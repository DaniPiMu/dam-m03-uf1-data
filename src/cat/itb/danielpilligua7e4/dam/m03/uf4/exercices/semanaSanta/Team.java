package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

public class Team {
    String Name;
    String Shout;

    public Team(String name, String shout) {
        Name = name;
        Shout = shout;
    }

    public String getName() {
        return Name;
    }

    public String getShout() {
        return Shout;
    }

    public void shoutMotto() {
        System.out.println(getShout());
    }
}
