package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices;

public class BycicleBrand {
    String name;
    String country;

    public BycicleBrand(String name, String country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "BycicleBrand{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

}
