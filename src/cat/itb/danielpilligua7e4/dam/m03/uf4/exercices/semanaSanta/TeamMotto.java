package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

import java.util.ArrayList;
import java.util.List;

public class TeamMotto {
    public static void main(String[] args) {
        List<Team> MottoTeams = new ArrayList<>();
        Team team1 = new Team("Mosques","Bzzzanyarem");
        MottoTeams.add(team1);
        Team team2 = new Team("Dragons","Grooarg");
        MottoTeams.add(team2);
        Team team3 = new Team("Abelles","Piquem Fort");
        MottoTeams.add(team3);
        shoutMottos(MottoTeams);
    }

    public static void shoutMottos(List<Team> mottoTeams) {
        for(Team team: mottoTeams)
            team.shoutMotto();
    }
}

