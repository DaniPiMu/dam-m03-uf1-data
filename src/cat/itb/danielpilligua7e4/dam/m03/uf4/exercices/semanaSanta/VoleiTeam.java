package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

public class VoleiTeam extends Team {
    String color;

    public VoleiTeam(String name, String shout, String color) {
        super(name, shout);
        this.color = color;
    }

    @Override
    public void shoutMotto() {
        System.out.println(getShout()+" " +color);
    }
}
