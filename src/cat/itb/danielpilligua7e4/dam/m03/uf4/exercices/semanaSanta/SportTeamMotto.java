package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices.semanaSanta;

import java.util.ArrayList;
import java.util.List;

public class SportTeamMotto {
    public static void main(String[] args) {
        List<Team> SportTeam = new ArrayList<>();
        Team team1 = new BasquetTeam("Mosques","Bzzzanyarem", "1,2,3" );
        SportTeam.add(team1);
        Team team2 = new VoleiTeam("Dragons", "Grooarg","verd");
        SportTeam.add(team2);
        Team team3 = new GolfTeam("Abelles","Piquem Fort", "Marta Ahuja");
        SportTeam.add(team3);
        shoutMottos(SportTeam);
    }

    private static void shoutMottos(List<Team> sportTeam) {
        for(Team team: sportTeam)
            team.shoutMotto();
    }
}
