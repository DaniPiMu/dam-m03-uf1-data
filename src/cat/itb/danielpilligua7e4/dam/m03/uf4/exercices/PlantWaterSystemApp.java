package cat.itb.danielpilligua7e4.dam.m03.uf4.exercices;

public class PlantWaterSystemApp {
    public static void main(String[] args) {
        PlantWaterMock plantWaterMock = new PlantWaterMock();

        PlantWaterControler plantWaterControl = new PlantWaterControler(plantWaterMock);
        plantWaterControl.waterIfNeeded();
    }
}
