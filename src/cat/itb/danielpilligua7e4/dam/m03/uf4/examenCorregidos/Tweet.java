package cat.itb.danielpilligua7e4.dam.m03.uf4.examenCorregidos;

public class Tweet {
    String usuari;
    String data;
    String text;

    public Tweet(String usuari, String data, String text) {
        this.usuari = usuari;
        this.data = data;
        this.text = text;
    }

    public String getUsuari() {
        return usuari;
    }

    public String getData() {
        return data;
    }

    public String getText() {
        return text;
    }

    public void print(){
        System.out.printf("@%s · %s\n", usuari, data);
        System.out.println(text);
        System.out.println();
    }

}

