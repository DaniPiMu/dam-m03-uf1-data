package cat.itb.danielpilligua7e4.dam.m03.uf4.examenCorregidos;

import java.util.List;

public class PollTweet extends Tweet {
    List<Voto> votos;

    public PollTweet(String usuari, String data, String text, List<Voto> votos) {
        super(usuari, data, text);
        this.votos = votos;
    }

    public List<Voto> getVotos() {
        return votos;
    }

    public void vote(int index) {
        votos.get(index).voto();
    }

    @Override
    public void print() {
        super.print();
        int totalVotes = getTotalVotes();
        for (Voto voto : votos){
            System.out.printf("- (%d/%d) %s\n", voto.getVoto(), totalVotes, voto.getText());
        }
    }

    private int getTotalVotes() {
        int count = 0;
        for(Voto voto : votos){
            count+=voto.getVoto();
        }
        return count;
    }

}
