package cat.itb.danielpilligua7e4.dam.m03.uf4.examenCorregidos;

public class Voto {
    String text;
    int voto;

    public Voto(String text, int voto) {
        this.text = text;
        this.voto = voto;
    }

    public String getText() {
        return text;
    }

    public int getVoto() {
        return voto;
    }

    public void voto() {
        this.voto++;
    }
}
