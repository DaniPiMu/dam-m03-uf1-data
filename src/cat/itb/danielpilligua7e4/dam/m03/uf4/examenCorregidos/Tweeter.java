package cat.itb.danielpilligua7e4.dam.m03.uf4.examenCorregidos;

import java.util.ArrayList;
import java.util.List;

public class Tweeter {
    public static void main(String[] args) {
        List<Tweet> Tweets = new ArrayList<>();
        Tweet tweet1 = new Tweet("imdevloper","07 de gener","Remember, a few hours of trial and error can save you several minutes of looking at the README");
        Tweet tweet2 = new Tweet("softcatala","29 de març","Avui mateix, #CommonVoiceCAT segueix creixent 🚀:\n🗣️ 856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!"  );
        Tweets.add(tweet1);
        Tweets.add(tweet2);


        List<Voto> votos = new ArrayList<>();
        votos.add(new Voto("Comèdia dramàtica - La Fúmiga",0));
        votos.add(new Voto("In the night - Oques Grasses",0));
        votos.add(new Voto("Una Lluna a l'Aigua - Txarango",0));
        votos.add(new Voto("Esbarzers - La Gossa Sorda",0));

        PollTweet tweet3 = new PollTweet("musicat","02 d'abril","Quina cançó t'agrada més?",votos);
        Tweets.add(tweet3);

        poolTweet(tweet3);
        print(Tweets);

    }

    private static void print(List<Tweet> tweets) {
        for(Tweet tweet:tweets)
            tweet.print();
    }

    private static void poolTweet(PollTweet tweet3) {
        tweet3.vote(0);
        tweet3.vote(0);
        tweet3.vote(0);
        tweet3.vote(1);
        tweet3.vote(1);
        tweet3.vote(2);
        tweet3.vote(2);
        tweet3.vote(3);
        tweet3.vote(3);
        tweet3.vote(3);
        tweet3.vote(3);

    }
}
