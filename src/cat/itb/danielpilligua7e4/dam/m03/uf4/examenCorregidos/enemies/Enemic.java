package cat.itb.danielpilligua7e4.dam.m03.uf4.examenCorregidos.enemies;

public class Enemic {
    String nom;
    int vida;

    public Enemic( String nom, int vida) {
        this.nom = nom;
        this.vida = vida;
    }

    public String getNom() {
        return nom;
    }

    public int getVida() {
        return vida;
    }

    public void attack(int i) {
        if(vida==0) {
            System.out.printf("L'enemic %s ja està mort%n", nom);
        }

        }

    @Override
    public String toString() {
        return "Enemic{" +
                "nom='" + nom + '\'' +
                ", vida=" + vida +
                '}';
    }
}
