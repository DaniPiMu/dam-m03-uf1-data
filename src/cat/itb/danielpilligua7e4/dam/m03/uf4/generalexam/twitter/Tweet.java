package cat.itb.danielpilligua7e4.dam.m03.uf4.generalexam.twitter;

public class Tweet {
    String usuari;
    String data;
    String text;

    public Tweet(String usuari, String data, String text) {
        this.usuari = usuari;
        this.data = data;
        this.text = text;
    }

    @Override
    public String toString() {
        return "@"+usuari + " · " + data + "\n" + text + "\n\n";
    }
}

