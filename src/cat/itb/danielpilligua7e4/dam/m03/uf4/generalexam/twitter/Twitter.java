package cat.itb.danielpilligua7e4.dam.m03.uf4.generalexam.twitter;

import java.util.ArrayList;
import java.util.List;

// a la hora de añadir el "tweet poll" a la lista, cuando voy a imprimir, no se imprime, pero encambio si lo imprimo con la funcion hecha de "print" si.
//Debajo del sout, esta comentada la funcion "print" con el tweet para que se vea que se imprime

public class Twitter {
    public static void main(String[] args) {

        List<Tweet> list = new ArrayList<>();

        Tweet tweet1 = new Tweet("iamdevloper", "07 de gener", "Remember, a few hours of trial and error can save you several minutes of looking at the README ");
        list.add(tweet1);

        Tweet tweet2 = new Tweet("softcatala",  "29 de març", "Avui mateix, #CommonVoiceCAT segueix creixent 🚀:\n🗣️ 856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!");
        list.add(tweet2);

        List<PollOption> poll = readPoll();
        PollTweet pollTweet3 = new PollTweet("musicat", "02 d'abril", "Quina cançó t'agrada més?",poll);
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(1);
        pollTweet3.vote(1);
        pollTweet3.vote(2);
        pollTweet3.vote(2);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        list.add(pollTweet3);

        Tweet tweet4 = new Tweet("ProgrammerJokes","05 d'abril","Q: what's the object-oriented way to become weathy?\n\nA: Inheritance");
        list.add(tweet4);

        System.out.println(list);

        //pollTweet3.print();

    }
    private static List<PollOption> readPoll() {
        List<PollOption> poll = new ArrayList<>();
        poll.add(new PollOption("Comèdia dramàtica - La Fúmiga"));
        poll.add(new PollOption("In the night - Oques Grasses"));
        poll.add(new PollOption("Una Lluna a l'Aigua - Txarango"));
        poll.add(new PollOption("Esbarzers - La Gossa Sorda"));
        return poll;
    }

}
