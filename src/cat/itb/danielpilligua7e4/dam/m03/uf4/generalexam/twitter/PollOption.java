package cat.itb.danielpilligua7e4.dam.m03.uf4.generalexam.twitter;

public class PollOption {
    int votos = 0;
    int totalVotos = 0;
    String text;

    public PollOption(String text) {
        this.text = text;
    }

    public int getVotes() {
        return votos;
    }

    public int getTotalVotes() {
        return totalVotos;
    }

    public void setVotes(int votes) {
        this.votos = votes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotos = totalVotes;
    }

    public void printOption(){
        if (totalVotos == 0){
            System.out.println(text);
        } else {
            System.out.println("- ("+votos+"/"+totalVotos+")"+ text +"\n");
        }
    }

}
