package cat.itb.danielpilligua7e4.dam.m03.uf4.generalexam.twitter;

import java.util.List;

public class PollTweet extends Tweet {
    List<PollOption> encuesta;

    public PollTweet(String usuari, String data, String text, List<PollOption> encuesta) {
        super(usuari, data, text);
        this.encuesta = encuesta;
}

    public void vote(int index){
        PollOption option = encuesta.get(index);
        option.setVotes(option.getVotes() + 1);
        for (PollOption pollOption : encuesta){
            pollOption.setTotalVotes(pollOption.getTotalVotes() + 1);
        }
    }
    public void print(){
        System.out.println("@"+usuari + " · " + data + "\n" + text + "\n\n");
        printPoll();
    }
    public void printPoll() {
        for (PollOption option : encuesta){
            option.printOption();
        }
    }


}

