package cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FromAvgSallaryInfo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // reader de empleado + y lista de empleados
        List<Trabajador> employees = readerEmployees(scanner);
        // avg salary
        List<Trabajador> underAvg = getUnderAverage(employees);

        //print();
        System.out.print(underAvg);

        ///////////////////////////////////////////////////////////////////////////////////////

    }


    private static List<Trabajador> getUnderAverage(List<Trabajador> employees) {
        double averageSalary= calcularAvg(employees);
        return lowerSalary(employees,averageSalary);
    }

    private static List<Trabajador> lowerSalary(List<Trabajador> employees, double averageSalary) {
        List<Trabajador> list = new ArrayList<>();
        for (Trabajador employee: employees){
            if (employee.getSalary()<averageSalary){
                list.add(employee);
            }
        }
        return list;
    }

    private static double calcularAvg(List<Trabajador> employees) {
        double suma = 0;
        for (Trabajador employee : employees){
            suma = suma + employee.getSalary();
        }
        return suma/ employees.size();
    }

    private static List<Trabajador> readerEmployees(Scanner scanner) {
        List<Trabajador> employees = new ArrayList<>();
        int size = scanner.nextInt();
        for (int i = 0; i<size;i++){
            Trabajador employee = readerEmployee(scanner);
            employees.add(employee);
        }
        return employees;
    }

    private static Trabajador readerEmployee(Scanner scanner){
        double salary = scanner.nextDouble();
        String name = scanner.next();
        return new Trabajador(salary, name);
    }
}
