package cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class OneProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String objectName = scanner.next();
        double price = scanner.nextDouble();

        Product product = new Product(objectName, price);
        System.out.print("el producte " + product.getName() +" val "+ product.getPrice()+"€");
    }
}
