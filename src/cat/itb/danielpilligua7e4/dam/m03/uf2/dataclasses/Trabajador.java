package cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses;

public class Trabajador {
    double salary;
    String name;

    public Trabajador(double salary, String name) {
        this.salary = salary;
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
