package cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

import static cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses.Funciones.printProduct;

public class MultiProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userIput = scanner.nextInt();

        //un producto
        for (int i = 0; i < userIput; i++) {
            Product reader = Funciones.readProduct(scanner);
            printProduct(reader);
        }
    }
}
