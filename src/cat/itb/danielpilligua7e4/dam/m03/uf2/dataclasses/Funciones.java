package cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class Funciones {
    public static void printProduct(Product product) {
        String name = product.getName();
        double price = product.getPrice();
        System.out.printf("El producte %s val %.2f€", name, price);
    }
    public static Product readProduct(Scanner scanner){
        String name = scanner.next();
        double price = scanner.nextDouble();
        return new Product(name,price);
    }

}
