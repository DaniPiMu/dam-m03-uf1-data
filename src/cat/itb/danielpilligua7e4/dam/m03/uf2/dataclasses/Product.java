package cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses;

public class Product {
    String name;
    double price;

    public Product (String name, double price){
        this.name = name;
        this.price = price;

    }
    public String getName(){
        return name;
    }
    public double getPrice(){
        return price;
    }
}
