package cat.itb.danielpilligua7e4.dam.m03.uf2.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AirContamination {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    //El usuario dice el tamaño de la lista y introduce la temperatura
    List<Integer> regTemp = new ArrayList<>();
    int size = scanner.nextInt();
        for (int i = 0; i < size; i++) {
            int temp = scanner.nextInt();
            regTemp.add(temp);
        }
    avgcontamination(regTemp);
    contadorDiasContaminacion(regTemp);
    printAVG(regTemp);
    printDias(regTemp);

}

    private static void printDias(List<Integer> regTemp) {
        System.out.printf("Dies amb amb contaminació superior: %d", contadorDiasContaminacion(regTemp));
    }

    private static void printAVG(List<Integer> regTemp) {
        System.out.println("Contaminació mitjana:" + avgcontamination(regTemp));
    }

    private static int contadorDiasContaminacion(List<Integer> regTemp) {
        //si alguna temperatura supera la media, contador +1
        int contador = 0;

        for (int i = 0; i < regTemp.size(); i++) {
            if (regTemp.get(i)<avgcontamination(regTemp)){
                contador ++;
            }
        } return contador;
    }

    private static double avgcontamination(List<Integer> regTemp) {
        //la suma de todos las temperaturas
        double contador=0;
        for (int i = 0; i < regTemp.size(); i++) {
            contador = contador + regTemp.get(i);
        }
        // la media
        double avg = contador /regTemp.size();
        return avg;
    }
}
