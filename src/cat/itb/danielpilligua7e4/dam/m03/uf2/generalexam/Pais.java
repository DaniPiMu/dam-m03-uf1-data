package cat.itb.danielpilligua7e4.dam.m03.uf2.generalexam;

public class Pais {
    String countryName;
    int casos;
    double gravetat;


    public Pais(String countryName, int casos, double gravetat) {
        this.countryName = countryName;
        this.casos = casos;
        this.gravetat = gravetat;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getCasos() {
        return casos;
    }

    public double getGravetat() {
        return gravetat;
    }
    public int puntuacio(){ return (int) (casos*gravetat);}


    public String toString() {
        return String.format("%s - casis: %d - gravetat: %f - puntuacio:%d", countryName, casos, gravetat, puntuacio());
    }



}
