package cat.itb.danielpilligua7e4.dam.m03.uf2.classfun;

public class LibraryBooks {
    String titol;
    String autor;
    int paginas;

    public LibraryBooks(String titol, String autor, int paginas) {
        this.titol = titol;
        this.autor = autor;
        this.paginas = paginas;
    }

    public String getTitol() {
        return titol;
    }

    public String getAutor() {
        return autor;
    }

    public int getPaginas() {
        return paginas;
    }
}

