package cat.itb.danielpilligua7e4.dam.m03.uf2.classfun;

import java.util.Scanner;

public class PeopleCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        int contador =0;
        while (userInput!=-1){
            contador = contador +userInput;
            userInput = scanner.nextInt();
        }
        System.out.println(contador);
    }
}
