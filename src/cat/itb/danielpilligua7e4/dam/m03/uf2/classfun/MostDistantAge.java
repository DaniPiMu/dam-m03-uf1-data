package cat.itb.danielpilligua7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MostDistantAge {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        List<Integer> ageList = new ArrayList<>();
        while (userInput != -1) {
            ageList.add(userInput);
            userInput = scanner.nextInt();
        }
        int max = 0;
        for (int i = 0; i < ageList.size(); i++)
            if (ageList.get(i) > max) {
                max = ageList.get(i);
            }
        int min = max;
        for (int i = 0; i < ageList.size(); i++)
            if (ageList.get(i) < min) {
                min =ageList.get(i);
            }
        int result = max - min;
        System.out.println("maximo " +max);
        System.out.println("minimo " +min);
        System.out.println("La diferencia es " +result);
    }
}
