package cat.itb.danielpilligua7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LibraryBooksApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // leer libros
        List <LibraryBooks> booksList = readerBooks(scanner);
        // imprimir informacion
        printLibraryData(booksList);



    }

    private static void printLibraryData(List<LibraryBooks> booksList) {
        System.out.println("Llibres");
        System.out.println("------------");
        printBooks(booksList);
        System.out.println("------------");
        //printBookResumee(booksList);
    }

    private static void printBooks(List<LibraryBooks> booksList) {
        String titol;
       // String autor=LibraryBooks.getAutor;
        int paginas;
       // System.out.printf("%s - %s - %d pàgines",titol,autor,paginas);
    }

    private static List<LibraryBooks> readerBooks(Scanner scanner) {
        List<LibraryBooks> booksList = new ArrayList<>();
        int size = scanner.nextInt();
        for (int i=0; i<size;++i){
            LibraryBooks book = readerBook(scanner);
            booksList.add(book);
        }
        return booksList;
    }

    private static LibraryBooks readerBook(Scanner scanner) {
        String titol = scanner.next();
        String autor= scanner.next();
        int paginas = scanner.nextInt();
        return new LibraryBooks(titol,autor,paginas);
    }
}
