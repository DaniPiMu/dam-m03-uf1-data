package cat.itb.danielpilligua7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RectangleSize {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Rectangle> rectangles= readRectangles(scanner);
        printRectangles(rectangles);
    }
    public static void printRectangles(List<Rectangle> rectangles) {
        for (Rectangle rectangle:rectangles){
            printRectangle(rectangle);
        }
    }

    private static void printRectangle(Rectangle rectangle) {
        double width = rectangle.getWidth();
        double height = rectangle.getHeight();
        double area = rectangle.getArea();
        System.out.printf("Un rectangle de %.1f x %.1f té %.1f d'area.\n",width, height, area);
    }

    public static List<Rectangle> readRectangles(Scanner scanner) {
        List<Rectangle> rectangleList = new ArrayList<>();
        int size = scanner.nextInt();
        for (int i=0;i<size;++i){
            Rectangle rectangle = readRectangle(scanner);
            rectangleList.add(rectangle);
        }
        return rectangleList;
    }

    public static Rectangle readRectangle(Scanner scanner) {
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();
        return new Rectangle(width,height);
    }
}