package cat.itb.danielpilligua7e4.dam.m03.uf2.classfun;

public class Rectangle {
    double width;
    double height;


    public double getWidth(){
        return width;
    }
    public double getHeight(){
        return height;
    }


    public Rectangle(double width, double height){
        this.width=width;
        this.height =height;
    }

    public double getArea() {
        return height*width;
    }
}
