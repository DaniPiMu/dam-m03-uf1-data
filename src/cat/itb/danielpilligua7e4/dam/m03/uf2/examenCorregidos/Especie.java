package cat.itb.danielpilligua7e4.dam.m03.uf2.examenCorregidos;

public class Especie {
    String nomCientific;
    String nomComu;
    int contador;


    public Especie(String nomCientific, String nomComu, int contador) {
        this.nomCientific = nomCientific;
        this.nomComu = nomComu;
        this.contador = contador;
    }

    public String getNomCientific() {
        return nomCientific;
    }

    public String getNomComu() {
        return nomComu;
    }

    public int getContador() {
        return contador;
    }

    @Override
    public String toString() {
        return nomCientific + " - " +nomComu + ":" + contador;
    }
}
