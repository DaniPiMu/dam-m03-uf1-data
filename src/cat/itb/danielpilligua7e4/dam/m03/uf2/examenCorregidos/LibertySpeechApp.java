package cat.itb.danielpilligua7e4.dam.m03.uf2.examenCorregidos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LibertySpeechApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Pais> listaPaises = readPaises(scanner);
        printResult(listaPaises);
        }

    private static void printResult(List<Pais> listaPaises) {
        System.out.println("---------- Paisos ----------");
        System.out.println(listaPaises);
        System.out.println("---------- Resum ----------");
        casosTotales(listaPaises);
        System.out.println(casosTotales(listaPaises));
        paisesMasCasos(listaPaises);
        System.out.println(paisesMasCasos(listaPaises));

    }

    private static String paisesMasCasos(List<Pais> listaPaises) {
        String paisMasCasos = null;
        int masCasos = 0;
        for (int i = 0; i < listaPaises.size(); i++) {
            if (listaPaises.get(i).getCasos()> masCasos){
                masCasos = listaPaises.get(i).getCasos();
                paisMasCasos = listaPaises.get(i).getNom();
            }
        } return paisMasCasos;
    }

    private static int casosTotales(List<Pais> listaPaises) {
        int sumaCasosTotales = 0;
        for (int i = 0; i < listaPaises.size(); i++) {
            sumaCasosTotales = listaPaises.get(i).getCasos() + sumaCasosTotales;
        }
        return sumaCasosTotales;
    }

    private static List<Pais> readPaises(Scanner scanner) {
        List<Pais> listaPaises = new ArrayList<>();
        int numPaies = scanner.nextInt();
        for (int i = 0; i < numPaies; i++) {
            Pais newPais = Readpais(scanner);
            listaPaises.add(newPais);
        }
        return listaPaises;
    }

    private static Pais Readpais(Scanner scanner) {
        String nom = scanner.next();
        int casos = scanner.nextInt();
        double gravetat = scanner.nextDouble();
        return new Pais(nom,casos,gravetat);
    }
}
