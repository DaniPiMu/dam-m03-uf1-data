package cat.itb.danielpilligua7e4.dam.m03.uf2.examenCorregidos;

public class Avistament {
    int posicionAnimal;
    int cantidad;

    public Avistament(int posicionAnimal, int cantidad) {
        this.posicionAnimal = posicionAnimal;
        this.cantidad = cantidad;
    }

    public int getPosicionAnimal() {
        return posicionAnimal;
    }

    public int getCantidad() {
        return cantidad;
    }

    @Override
    public String toString() {
        return posicionAnimal + " " +cantidad ;
    }
}
