package cat.itb.danielpilligua7e4.dam.m03.uf2.examenCorregidos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EndangeredAnimalSights {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Especie> listEspecie = readEspecies(scanner);
        List<Avistament> listAvistament = readAvistaments(scanner, listEspecie);
        printAvistament(listEspecie,listAvistament);
        printResum(listEspecie);
    }

    private static void printAvistament(List<Especie> listEspecie, List<Avistament> listAvistament) {
        System.out.println("--- Avistaments ---");
        for (int i = 0; i <listAvistament.size() ; i++) {
            System.out.printf("%s : %d \n",listEspecie.get(listAvistament.get(i).getPosicionAnimal()).getNomCientific(),listAvistament.get(i).getCantidad());
        }
    }

    private static void printResum( List<Especie> listEspecie) {
        System.out.println("--- Resum ---");
        System.out.println(listEspecie );
    }

    private static List<Avistament> readAvistaments(Scanner scanner,List<Especie> especies ) {
        List<Avistament> listaAvistament = new ArrayList<>();
        int userValue = scanner.nextInt();
        for (int i = 0; i < userValue; i++) {
            Avistament avistamiento = readAvistament(scanner, especies);
            listaAvistament.add(avistamiento);
        }
        return listaAvistament;
    }

    private static Avistament readAvistament(Scanner scanner, List<Especie> especies) {
        int posicionAnimal = scanner.nextInt();
        int cantidad = scanner.nextInt();
        especies.get(posicionAnimal).contador += cantidad;
        return new Avistament(posicionAnimal,cantidad);
    }

    private static List<Especie> readEspecies(Scanner scanner) {
        List<Especie> listaEspecies=new ArrayList<>();
        int userValue = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < userValue; i++) {
            Especie especie = readEspeies(scanner);
            listaEspecies.add(especie);
        }
        return listaEspecies;
    }

    private static Especie readEspeies(Scanner scanner) {
        String nomCientific = scanner.nextLine();
        String nomComu = scanner.nextLine();
        return new Especie(nomCientific,nomComu,0);
    }


}
