package cat.itb.danielpilligua7e4.dam.m03.uf2.examenCorregidos;

public class Pais {
    String nom;
    int casos;
    double gravetat;

    public Pais(String nom, int casos, double gravetat) {
        this.nom = nom;
        this.casos = casos;
        this.gravetat = gravetat;
    }

    public String getNom() {
        return nom;
    }

    public int getCasos() {
        return casos;
    }

    public double getGravetat() {
        return gravetat;
    }

    @Override
    public String toString() {
        return nom + " - casos: " + casos + "- gravetat: " + gravetat + " - puntuacio: " + (casos*gravetat) +"\n";
    }
}
