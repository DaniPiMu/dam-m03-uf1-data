package cat.itb.danielpilligua7e4.dam.m03.uf2.examenCorregidos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AirContamination {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> contaminacionValues = readValues(scanner);
        printResults(contaminacionValues);
    }

    private static void printResults(List<Integer> contaminacionValues) {
        avgValue(contaminacionValues);
        moreContaminationDays(contaminacionValues);
        System.out.println("Contaminació mitjana: " +avgValue(contaminacionValues));
        System.out.println("Dies amb contaminació superior: " +moreContaminationDays(contaminacionValues));
    }

    private static int moreContaminationDays(List<Integer> contaminacionValues) {
        int contador = 0;
        for (int i = 0; i <contaminacionValues.size() ; i++) {
            if (contaminacionValues.get(i)> avgValue(contaminacionValues)){
                contador =  contador +1;
            }
        }
        return contador;
    }

    private static int avgValue(List<Integer> contaminacionValues) {
        int sumaValores = 0;
        for (int i = 0; i < contaminacionValues.size(); i++) {
            int valor = contaminacionValues.get(i);
            sumaValores =sumaValores+ valor;
        }
        int avg = sumaValores/ contaminacionValues.size();
        return avg;
    }

    private static List<Integer> readValues(Scanner scanner) {
        int contaminacionValue= scanner.nextInt();
        List<Integer> contaminacionValues = new ArrayList<>();
        while (contaminacionValue != -1){
            contaminacionValues.add(contaminacionValue);
            contaminacionValue = scanner.nextInt();
        }
        return contaminacionValues;
    }
}
