package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class StudentStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> statsList = IntegerLists.readIntegerList(scanner);
        int minNote = IntegerLists.min(statsList);
        int maxNote = IntegerLists.max(statsList);
        double avgNote = IntegerLists.avg(statsList);
        System.out.println("Nota mínima: " + minNote);
        System.out.println("Nota màxima: " + maxNote);
        System.out.println("Nota mitjana " + avgNote);
    }
}
