package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<>();
        int userInput = scanner.nextInt();
        while (userInput!=-1){
            list.add(userInput);
            userInput= scanner.nextInt();
        }
        return list;
    }
    public static int min(List<Integer> list){
        int minValue = list.get(0);
        for (int integer:list){
            minValue = Math.min(minValue,integer);
        }
        return minValue;
    }
    public static int max(List<Integer> list){
        int maxValue = list.get(0);
        for (int integer:list){
            maxValue = Math.max(maxValue,integer);
        }
        return maxValue;
    }
    public static double avg(List<Integer> list){
        double contador = 0;
        for (int Integer:list){
            contador+= Integer;
        }
        return contador/ list.size();
    }
    public static List<Integer> readFromPath(Path path) throws IOException {
        List<Integer> list = new ArrayList<>();
        Scanner filesSanner = new Scanner(path);
        while(filesSanner.hasNext()){
            int speed = filesSanner.nextInt();
            list.add(speed);
        }
        return list;
    }
}
