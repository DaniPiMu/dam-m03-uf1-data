package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CheapestPrice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> priceList = IntegerLists.readIntegerList(scanner);
        int minPrice = IntegerLists.min(priceList);
        System.out.println("El producte més econòmic val: " + minPrice + "€");
    }
}
