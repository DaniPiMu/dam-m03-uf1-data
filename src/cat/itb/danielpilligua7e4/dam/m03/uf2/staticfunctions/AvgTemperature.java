package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class AvgTemperature {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> tempList = IntegerLists.readIntegerList(scanner);
        double avgTemperature = IntegerLists.avg(tempList);
        System.out.println("Ha fet " + avgTemperature + " graus de mitjana");
    }
}
