package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

/*
Volem fer una petita aplicació per fer estadístiques de les dades de les infeccions de COVID a la nostre ciutat. Una centre d'investigació ens ha passat una classe amb diferents funcions que podeu descarregar aquí: CovidCalculations.java

Fes un programa que, donat una llista de casos diaris imprimeixi la informació de:

nombre de casos total
nombre de casos mitjà
última taxa de creixement
 */
public class CovidApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Nombre de casos total
        List<Integer> dailyCases = CovidCalculations.readDailyCasesFromScanner(scanner);
        int totalCases = CovidCalculations.countTotalCases(dailyCases);

        //Nombre de casos mitjà
        double avgCases = CovidCalculations.average(dailyCases);

        //Ultima taxa creixement
        List<Double> growthlist = CovidCalculations.growthRates(dailyCases);
        double growth = growthlist.get(growthlist.size()-1);

        System.out.printf("Hi ha hagut %d casos en total, amb una mitjana de %.2f per dia. L'ultim creixement es de %.2f", totalCases, avgCases, growth);
    }
}
