package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class OldestStudent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> studentsAge = IntegerLists.readIntegerList(scanner);
        int oldestStudents = IntegerLists.max(studentsAge);
        System.out.println("L'alumne més gran té " + oldestStudents+" anys");
    }
    }
