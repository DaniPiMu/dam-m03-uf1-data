package cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class IntegerListsReaderSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> readeList = IntegerLists.readIntegerList(scanner);
        System.out.println(readeList);
    }
}
