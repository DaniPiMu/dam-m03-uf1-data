package cat.itb.danielpilligua7e4.dam.m03.uf2.practica;

import java.util.*;

public class HighPeaks {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        //Leer informacion picos
        List<Peak> peakList = readerPeaks(scanner);

        //Imprimimos
        printHeightPeaks(peakList);
    }

    private static void printHeightPeaks(List<Peak> peakList) {
        System.out.println("------------------------");
        System.out.println("--- Cims aconseguits ---");
        System.out.println("------------------------");
        printPeaks(peakList);
        System.out.println("------------------------");
        printTotalPeaks(peakList);
        printHeightPeak(peakList);
        printQuickPeak(peakList);
        printFastPeak(peakList);
        System.out.println("------------------------");

    }

    private static void printFastPeak(List<Peak> peakList) {
        Peak fastPeak = peakList.get(0);
        for (Peak peak : peakList){
            if (peak.printAvg()>fastPeak.printAvg()){
                fastPeak = peak;
            }
        }
        System.out.printf("Cim més veloç: %s %skm/hora \n",fastPeak.getPeakName(),String.format("%.2f", fastPeak.printAvg()));
    }

    private static void printQuickPeak(List<Peak> peakList) {
        Peak quickPeak = peakList.get(0);
        for (Peak peak : peakList){
            if (peak.getSeconds()<quickPeak.getSeconds()){
                quickPeak = peak;
            }
        }
        System.out.printf("Cim més ràpid: %s (%s) \n",quickPeak.getPeakName(), quickPeak.secondsToMinutes() );
    }

    private static void printTotalPeaks(List<Peak> peakList) {
        int totalPeaks = getTotalPeaks(peakList);
        System.out.println("N. cims: " + totalPeaks);
    }

    private static int getTotalPeaks(List<Peak> peakList) {
        int total =0;
        for (Peak peak: peakList){
            total++;
        }
        return total;
    }
    /* los nombres compuestos separados por un espacio, solo se imprime el primer nombre.*/
    private static void printHeightPeak(List<Peak> peakList) {
        Peak hightestPeak = peakList.get(0);
        for (Peak peak : peakList){
            if (peak.getHeight()>hightestPeak.getHeight()){
                hightestPeak = peak;
            }
        }
        System.out.printf("Cim més alt: %s (%dm) \n",hightestPeak.getPeakName(), hightestPeak.getHeight() );
    }

    private static void printPeaks(List<Peak> peakList) {
        for (Peak peak: peakList){
            printPeak(peak);
        }
    }

    private static void printPeak(Peak peak) {
        System.out.println(peak);
    }

    /**
     * read a peak from readerPeak
     * @param scanner
     * @return a peak list.
     */
    private static List<Peak> readerPeaks(Scanner scanner) {
        List<Peak> peakList = new ArrayList<>();
        int size = scanner.nextInt();
        for (int i=0;i<size;++i){
            Peak peak = readerPeak(scanner);
            peakList.add(peak);
            scanner.nextLine();
        }
        return peakList;
    }

    /**
     * read information about the peak
     * @param scanner
     * @return a peak
     */
    private static Peak readerPeak(Scanner scanner) {
        String peakName = scanner.next();
        scanner.nextLine();
        int height= scanner.nextInt();
        String country=scanner.next();
        scanner.nextLine();
        int distance = scanner.nextInt();
        int seconds = scanner.nextInt();
        return new Peak(peakName,height,country,distance,seconds);
    }
}
