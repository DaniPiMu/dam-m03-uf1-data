package cat.itb.danielpilligua7e4.dam.m03.uf2.practica;

public class Peak {
    String peakName;
    int height;
    String country;
    int distance;
    int seconds;

    public Peak(String peakName, int height, String country, int distance, int seconds) {
        this.peakName = peakName;
        this.height = height;
        this.country = country;
        this.distance = distance;
        this.seconds = seconds;
    }


    public String getPeakName() {
        return peakName;
    }

    public int getHeight() {
        return height;
    }

    public String getCountry() {
        return country;
    }

    public int getDistance() {
        return distance;
    }

    public int getSeconds() {
        return seconds;
    }

    @Override

    public String toString() {
        return String.format("%s - %s (%dm) - Temps: %s", peakName, country,height, secondsToMinutes());
    }
    public String secondsToMinutes()
    {
        int horas = (seconds / 3600);
        int minutos = ((seconds-horas * 3600)/60);
        int segundos = seconds-(horas * 3600+minutos*60);
        return minutos + ":" + segundos;
    }

    public double printAvg() {
        double time = getSeconds();
        double km = getDistance();
        return (km/1000)/ (time/60);
    }

}
