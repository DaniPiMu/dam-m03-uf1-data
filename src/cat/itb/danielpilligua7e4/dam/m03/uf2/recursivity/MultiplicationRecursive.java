package cat.itb.danielpilligua7e4.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class MultiplicationRecursive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int result = multiply(n,m);
        System.out.println(result);
    }
    // n*3 = n*2+n

    public static int multiply(int n, int m){
        if (m==1){
            return n;
        }else return n+ multiply(n, m-1);
    }
}
