package cat.itb.danielpilligua7e4.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class DotLineRecursive {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int userValue = scanner.nextInt();
    String line =lineRecursive(userValue);
        System.out.println(line);
    }
    private static String lineRecursive(int userValue) {
        if (userValue ==0){
            return "";
        } else return "." + lineRecursive(userValue-1);
    }


}
