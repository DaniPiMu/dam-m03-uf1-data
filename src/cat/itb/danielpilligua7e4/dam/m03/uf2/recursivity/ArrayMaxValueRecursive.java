package cat.itb.danielpilligua7e4.dam.m03.uf2.recursivity;

import cat.itb.danielpilligua7e4.dam.m03.uf1.arrays.ArrayReader;

import java.util.List;
import java.util.Scanner;

public class ArrayMaxValueRecursive {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        ArrayReader.scannerReadIntArray(scanner);

        System.out.println();
    }
    public int max(List<Integer> array) {

        return max(array,0);
    }
    public static int max(List<Integer>array, int i) {
        if (i==array.size()-1)
            return array.get(i);
        int current = array.get(i);
        return Math.max(current, max(array, i+1));
    }
    }


