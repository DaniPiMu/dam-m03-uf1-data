package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class GenerateDummyFileStructure {
    public static void main(String[] args) throws IOException {
        Path folder = Paths.get("C:/Users/danu/Desktop","dummyfolders");
        Files.createDirectories(folder);
        createSubFolder(folder);

    }

    private static void createSubFolder(Path folder) throws IOException {
        for (int i = 1;i<100;i++){
            String name =i+"";
            Path subfolder = folder.resolve(name);
            Files.createDirectories(subfolder);
        }
    }
}
