package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Intro_Excepcions {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        //esto se utiliza para que el usuario no vea el error en rojo, para que lo vea "bonito"
        try{
            int inputValue = scanner.nextInt();
            System.out.println("Numero: "+ inputValue);
        } catch (InputMismatchException inputMismatchException){
            System.out.println("No es un enter");
        }

    }
}
