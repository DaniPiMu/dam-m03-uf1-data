package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class ProfileBackup {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path profilePath = Paths.get(homePath, ".profile");
        String date = LocalDateTime.now().toString();
        Path backupFolder = Paths.get(homePath, "backup", date);
        Files.createDirectories(backupFolder);
// "carpeta".resolve("nombre carpeta dentro de "carpeta"") lo que hace es añadir un direcotrio dentro de un directorio
        Path destination = backupFolder.resolve(".profile");
        Files.copy(profilePath, destination);
    }
}
