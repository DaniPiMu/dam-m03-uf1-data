package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class EssayAnalyser {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String stringScanner = scanner.nextLine();
        Path path = Paths.get(stringScanner);

        int lineCounter=0;
        Scanner lineScanner = new Scanner(path);
        while (lineScanner.hasNext()){
            lineScanner.nextLine();
            lineCounter++;
        }

        int wordCounter=0;
        Scanner wordScanner = new Scanner(path);
        while (wordScanner.hasNext()){
            wordScanner.next();
            wordCounter++;
        }
        System.out.println("Numero de palabras: " + wordCounter);
        System.out.println("Numero de lineas: " + lineCounter);
    }
}
