package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class IWasHere {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("C:/Users/Danieh/Desktop","i_was_here.txt");
        String txt="I Was Here :";
        String date = LocalDateTime.now().toString();
        String line = txt + date+"\n";
        Files.writeString(path,line, StandardOpenOption.APPEND);

    }
}
