package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.nio.file.Files;
import java.nio.file.Path;

public class FileExists {
    public static void main(String[] args) {
        Path path = Path.of("C:/Windows");
        boolean existe = Files.exists(path);
        System.out.println("existes = " + existe);
    }
}
