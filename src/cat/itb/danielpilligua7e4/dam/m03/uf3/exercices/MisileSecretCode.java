package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MisileSecretCode {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Path path = Paths.get("secret.txt");
        String code = scanner.nextLine();
        Files.writeString(path, code);
        System.out.println("missil preparat");
    }
}
