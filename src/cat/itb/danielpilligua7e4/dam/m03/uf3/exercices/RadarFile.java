package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import cat.itb.danielpilligua7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RadarFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Path path = Paths.get(scanner.nextLine());

        List<Integer> velocityList = new ArrayList<>();
        // creamos un scanner especifico del path para que lea su contendio
        Scanner fileScanner = new Scanner(path);

        // mientras que haya algo delante
        while (fileScanner.hasNext()){
            // guardamos el numero
            int speedValue = fileScanner.nextInt();
            velocityList.add(speedValue);
        }
        //Calculem la velocitat maxima, minima i mitjana
        double maxSpeed = IntegerLists.max(velocityList);
        double minSpeed = IntegerLists.min(velocityList);
        double averageSpeed = IntegerLists.avg(velocityList);

        //Imprimim les velocitats
        System.out.printf("Velocitat màxima: %.0fkm/h\n", maxSpeed);
        System.out.printf("Velocitat mínima: %.0fkm/h\n", minSpeed);
        System.out.printf("Velocitat mitjana: %.0fkm/h", averageSpeed);
    }
}
