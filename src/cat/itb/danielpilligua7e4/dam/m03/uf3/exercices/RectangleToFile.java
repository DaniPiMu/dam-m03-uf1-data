package cat.itb.danielpilligua7e4.dam.m03.uf3.exercices;

import cat.itb.danielpilligua7e4.dam.m03.uf2.classfun.Rectangle;
import cat.itb.danielpilligua7e4.dam.m03.uf2.classfun.RectangleSize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class RectangleToFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        List<Rectangle> rectangleList = RectangleSize.readRectangles(scanner);
        String fileString= scanner.nextLine();
        Path path = Paths.get(fileString);
        saveRectangle(rectangleList, path);
    }
    // esto es para que se guarden cada rectangulo en una linea
    private static void saveRectangle(List<Rectangle> rectangles, Path path) throws IOException {
        try (OutputStream outputStream = Files.newOutputStream(path)) {
            PrintStream printStream = new PrintStream(outputStream, true);
            for(Rectangle rectangle:rectangles){
                save(rectangle, printStream);
            }
        }
    }

    private static void save(Rectangle rectangle, PrintStream printStream) {
        double width = rectangle.getWidth();
        double height = rectangle.getHeight();
        double area = rectangle.getArea();
        printStream.printf("Un rectangle de %.1f x %.1f té %.1f d'area.", width, height, area);
    }


}
