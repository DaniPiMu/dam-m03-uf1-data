package cat.itb.danielpilligua7e4.dam.m03.uf3.generalexam;

import cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses.GradeCalculator;
import cat.itb.danielpilligua7e4.dam.m03.uf2.dataclasses.StudentGrades;
import cat.itb.danielpilligua7e4.dam.m03.uf3.exercices.RectangleSizesFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class StudentGradesFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<StudentGrades> student = readFromFile(scanner);
        GradeCalculator.printStudents(student);
    }

    private static List<StudentGrades> readFromFile(Scanner scanner) throws IOException {
        String stringPath = scanner.nextLine();
        Path path = Paths.get(stringPath);
        Scanner fileScanner = new Scanner(path);
        return GradeCalculator.readStudents(fileScanner);
    }
}
