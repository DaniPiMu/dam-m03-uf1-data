package cat.itb.danielpilligua7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class CopyToUserFolder {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        // lo que se tiene que copiar
        Path file = Paths.get("/.bashrc");

        // ruta introducida y nombre, apellido
        String path = scanner.nextLine();
        String name = scanner.nextLine();
        String lastName = scanner.nextLine();
        Path finallyPath = Paths.get(path, name, lastName);
        Files.move(file,finallyPath);
    }
}
