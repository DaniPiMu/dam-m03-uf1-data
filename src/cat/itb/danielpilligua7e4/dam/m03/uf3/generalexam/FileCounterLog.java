package cat.itb.danielpilligua7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class FileCounterLog {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String stringScanner = scanner.nextLine();
        Path path = Paths.get(stringScanner);

        //donde se creará el fichero
        String homePath = System.getProperty("user.home");
        Path counterlogPath = Paths.get(homePath, "counterlog.txt");


        String data_hora = LocalDateTime.now().toString();
        String finalytxt = data_hora +"tens " + "fitxers a " + path;

        // E aqui el intento de listar los fitchero de la ruta del usuario.
        try(Stream<Path>fileStream = Files.list(path)){
            List<Path> counter = fileStream.collect(toList());
        }

        Files.writeString(counterlogPath, finalytxt, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
    }
}
