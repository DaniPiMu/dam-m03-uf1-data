package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf4.gestiousuaris;

public class Usuari {
    String Nom;
    String Cognom;
    String segonCognom;
    String dataNaixement;

    public String getNom() {
        return Nom;
    }

    public String getCognom() {
        return Cognom;
    }

    public String getSegonCognom() {
        return segonCognom;
    }

    public String getDataNaixement() {
        return dataNaixement;
    }

    public Usuari(String nom, String cognom, String segonCognom, String dataNaixement) {
        Nom = nom;
        Cognom = cognom;
        this.segonCognom = segonCognom;
        this.dataNaixement = dataNaixement;

    }
    public void print(){
        System.out.printf("Nom: %s \n" +
                "Cognoms: %s %s \n" +
                "Data de naixement: %s \n" +
                "Correu electronic: %s.%s@itb.cat \n" , getNom(), getCognom(), getSegonCognom(), getDataNaixement(), getNom(), getCognom());


    }
}
