package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf4.filtre;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class FiltreText {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        List<Filter> textos = new ArrayList<>();

        textos.add(new UpperCaseFilter(text));
        textos.add(new ReverseFilter(text));
        printList(textos);
    }

    private static void printList(List<Filter> textos) {
        for (Filter filter : textos){
            filter.filterText(filter.text);
        }
    }




}
