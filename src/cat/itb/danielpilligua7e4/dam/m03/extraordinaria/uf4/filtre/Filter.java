package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf4.filtre;

public abstract class Filter {
    String text;

    public Filter(String text) {
        this.text = text;
    }
    public abstract void filterText(String text);

}
