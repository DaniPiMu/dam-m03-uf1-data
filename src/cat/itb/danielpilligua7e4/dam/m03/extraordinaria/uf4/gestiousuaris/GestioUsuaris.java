package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf4.gestiousuaris;

import java.util.ArrayList;
import java.util.List;

public class GestioUsuaris {
    public static void main(String[] args) {
        List<Usuari> infoUsers = new ArrayList<>();
        Usuari usuari1 = new Usuari("Dani", "Carrasco", "Morera", "04/09/1986");
        infoUsers.add(usuari1);
        Usuari usuari2 = new Usuari("Laura", "Ivars", "Perelló","17/05/1996" );
        infoUsers.add(usuari2);
        Estudiants estudiants1 = new Estudiants("Aitana", "Sanchis", "Mari", "20/11/2001", "2020");
        infoUsers.add(estudiants1);

        print(infoUsers);

    }

    private static void print(List<Usuari> infoUsers) {
        for (Usuari usuari:infoUsers) {
            usuari.print();

        }
    }
}
