package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf4.gestiousuaris;

public class Estudiants extends Usuari {
    String DataMatriculacio;

    public Estudiants(String nom, String cognom, String segonCognom, String dataNaixement, String dataMatriculacio) {
        super(nom, cognom, segonCognom, dataNaixement);
        DataMatriculacio = dataMatriculacio;
    }

    public String getDataMatriculacio() {
        return DataMatriculacio;
    }

    @Override
    public void print() {
        System.out.printf("Nom: %s \n" +
                "Cognoms: %s %s \n" +
                "Data de naixement: %s \n" +
                "Correu electronic: %s.%s.7e4@itb.cat \n", getNom(), getCognom(), getSegonCognom(), getDataNaixement(), getNom(), getCognom());
    }
}
