package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf2;

public class Albums {
    int artista;
    String nom;
    int any;

    public Albums(int artista, String nom, int any) {
        this.artista = artista;
        this.nom = nom;
        this.any = any;
    }

    public int getArtista() {
        return artista;
    }

    public String getNom() {
        return nom;
    }

    public int getAny() {
        return any;
    }

    @Override
    public String toString() {
        return "Albums{" +
                "artista=" + artista +
                ", nom='" + nom + '\'' +
                ", any=" + any +
                '}';
    }
}
