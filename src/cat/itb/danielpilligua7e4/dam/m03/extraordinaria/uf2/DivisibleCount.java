package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DivisibleCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int divisibilitat = scanner.nextInt();
        int userInput = scanner.nextInt();
        List<Integer> list = new ArrayList<>();
        while(userInput!=-1){
            list.add(userInput);
            userInput = scanner.nextInt();
        }
        System.out.println(list);
        int count = 0;
        int count_loops=0;
        for (int i = 0; i < divisibilitat; i++) {
            count_loops = count_loops+1;
            if (list.get(i) % divisibilitat == 0){
                count = count +1;
            }
            System.out.println(count_loops + ": " + count);
        }
    }
}
