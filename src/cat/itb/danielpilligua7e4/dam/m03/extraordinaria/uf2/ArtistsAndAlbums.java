package cat.itb.danielpilligua7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArtistsAndAlbums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Artist> artistList = artisReader(scanner);
        List<Albums> albumsList = albumsList(scanner);
        System.out.println(artistList);
        System.out.println(albumsList);
        printOutPut(artistList,albumsList);
    }

    private static void printOutPut(List<Artist> artistList, List<Albums> albumsList) {
        System.out.println("#### Albums");
        for (int i = 0; i < albumsList.size(); i++) {
            int x = albumsList.get(i).getArtista();
            // coger el numero de artista y coger la posicion que sea igual al numero

            System.out.printf(" %s (%s) - %s - %d \n",artistList.get(x).getNom(), artistList.get(x).getPais() , albumsList.get(i).getNom(), albumsList.get(i).getAny() );

        }
    }

    private static List<Albums> albumsList(Scanner scanner) {
        List<Albums> albumsList = new ArrayList<>();
        int albumNum = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < albumNum; i++) {
            Albums newAlbum = readAlbums(scanner);
            albumsList.add(newAlbum);
        }
        return albumsList;
    }

    private static Albums readAlbums(Scanner scanner) {
        int artista = scanner.nextInt();
        scanner.nextLine();
        String nom = scanner.nextLine();
        int any = scanner.nextInt();
        scanner.nextLine();
        return new Albums(artista,nom,any);
    }

    private static List<Artist> artisReader(Scanner scanner) {
        List<Artist> artistList = new ArrayList<>();
        int artistNum = scanner.nextInt();
        for (int i = 0; i < artistNum; i++) {
            Artist newArtist = readArtist(scanner);
            artistList.add(newArtist);
        }
        return artistList;
    }


    private static Artist readArtist(Scanner scanner) {
        String nom = scanner.next();
        String pais = scanner.next();
        return  new Artist(nom, pais);
    }
}
