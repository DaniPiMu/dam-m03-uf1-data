package cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.ui;

import cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.data.BookDao;
import cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.domain.Book;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class MainMenu {
    BookDao bookDao;
    Scanner scanner;

    public MainMenu(BookDao bookDao, Scanner scanner) {
        this.bookDao = bookDao;
        this.scanner = scanner;
    }

    public void menuDB() throws SQLException {
        System.out.printf(
                "---------------MENU---------------\n"+
                        "PERSONAS\n"+
                        "\t 1. Libros del año X%n" +
                        "\t 2. Insertar libros%n"+
                        "\t 3. Libro con mas paginas%n"+
                        "EXIT\n"+
                        "\t 0. Salir de la aplicación%n"+
                        "----------------------------------\n" );
        int operation = scanner.nextInt();

        switch (operation){
            case 1:
                ListBooksByYearApp();
                menuDB();
            case 2:
                InsertBookApp();
                menuDB();
            case 3:
                LongestBookApp();
                menuDB();
            case 0:
                System.exit(0);
        }
    }

    private void LongestBookApp() throws SQLException {
        List<Book> books = bookDao.longes();
        for (Book book: books) {
            System.out.printf("%s - %s - %d\n", book.getTitol(),book.getAutor(),book.getAny());
        }
    }

    private void InsertBookApp() throws SQLException {
        System.out.println("Introduce: titulo, autor, ISB, any, paginas");
        String titol = scanner.next();
        String autor = scanner.next();
        String ISBN = scanner.next();
        int any = scanner.nextInt();
        int paginas = scanner.nextInt();
        bookDao.insertBook(new Book(titol,autor,ISBN,any, paginas));
    }

    private void ListBooksByYearApp() throws SQLException {
        System.out.println("introduce el año");
        int input = scanner.nextInt();
        inputYearShowBook(input);
    }

    private void inputYearShowBook(int input) throws SQLException {
        Book book = bookDao.inputYearShowBook(input);
        if (book == null) {
            System.out.println("El año no existeix");
            ListBooksByYearApp();
        } else {
            System.out.println(book);
        }
        //estoy intentado el bucle pero no me sale
    }
    }
