package cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam;

import cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.data.BookDao;
import cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.data.BookDatabase;
import cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.ui.MainMenu;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class app {
    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        BookDatabase database = new BookDatabase();
        try(Connection connection = database.connect()) {
            BookDao bookDao = new BookDao(database);
            MainMenu menu = new MainMenu(bookDao, scanner);
            menu.menuDB();
        }
    }
}
