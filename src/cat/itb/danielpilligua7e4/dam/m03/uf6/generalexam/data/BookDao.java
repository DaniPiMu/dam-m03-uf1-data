package cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.data;

import cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.domain.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    BookDatabase database;

    public BookDao(BookDatabase database) {
        this.database = database;
    }

    public Connection getConnection(){
        return database.getConnection();
    }

    public List<Book> list() throws SQLException {
        String query = "SELECT * FROM book";
        Statement listStatement = getConnection().createStatement();
        ResultSet resultat = listStatement.executeQuery(query);
        return toBookList(resultat);
}

    private List<Book> toBookList(ResultSet resultat) throws SQLException {
        List<Book> books = new ArrayList<>();
        while (resultat.next()){
            String titol = resultat.getString("title");
            String autor = resultat.getString("author");
            String ISBN = resultat.getString("isbn");
            int any = resultat.getInt("year");
            int paginas = resultat.getInt("pages");
            Book book = new Book(titol, autor,ISBN,any, paginas);
            books.add(book);
        }
        return books;
    }

    public Book inputYearShowBook(int input) throws SQLException {
        String query = "SELECT * FROM book WHERE year = ?";
        PreparedStatement insertStatement = getConnection().prepareStatement(query);
        insertStatement.setInt(1,input);
        ResultSet resultSet = insertStatement.executeQuery();

        List<Book> resultat = toBookList(resultSet);
        if (resultat.isEmpty())
            return  null;
        return resultat.get(0);
    }

    public void insertBook(Book book) throws SQLException {
        String query = "insert into book values(?,?,?,?,?) ";
        PreparedStatement insertStatement = getConnection().prepareStatement(query);
        insertStatement.setString(1, book.getTitol());
        insertStatement.setString(2, book.getAutor());
        insertStatement.setString(3,book.getISBN());
        insertStatement.setInt(4, book.getAny());
        insertStatement.setInt(5,book.getPaginas());
        insertStatement.execute();
    }

    public List<Book> longes() throws SQLException {
        String query = "SELECT * FROM book ORDER BY pages DESC LIMIT 1";
        Statement listStatement = getConnection().createStatement();
        ResultSet resultat = listStatement.executeQuery(query);
        return  toBookList(resultat);
    }
}
