package cat.itb.danielpilligua7e4.dam.m03.uf6.generalexam.domain;

public class Book {
    String titol;
    String autor;
    String ISBN;
    int any;
    int paginas;

    public Book(String titol, String autor, String ISBN, int any, int paginas) {
        this.titol = titol;
        this.autor = autor;
        this.ISBN = ISBN;
        this.any = any;
        this.paginas = paginas;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public int getAny() {
        return any;
    }

    public void setAny(int any) {
        this.any = any;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    @Override
    public String toString() {
        return "Book{" +
                "titol='" + titol + '\'' +
                ", autor='" + autor + '\'' +
                ", ISBN='" + ISBN + '\'' +
                ", any=" + any +
                ", paginas=" + paginas +
                '}';
    }
}
