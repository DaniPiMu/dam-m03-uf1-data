package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

//Printa per pantalla tots els números fins a un enter entrat per l'usuari

import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        //introdueix un enter
        System.out.println("Introduce un numero");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        int i =0;
        while (i < value){
            i++;
            System.out.print(i);
        }
    }

}
