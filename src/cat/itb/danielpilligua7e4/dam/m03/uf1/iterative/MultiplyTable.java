package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

//Volem printar les taules de múltiplicar.

import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        //l'usuari introdueix un enter
        System.out.println("Introduce un numero");
        Scanner scanner = new Scanner(System.in);
        int value1 = scanner.nextInt();
        int i = 1;
        while (i < 9){
            System.out.println(i + " * " + (value1) + " = " + (i*value1));
            i++;

        }
    }
}
