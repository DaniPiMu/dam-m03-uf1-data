package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class SetIMig {
    public static void main(String[] args) {
        playUserTurn();
    }

    /**
     * Given the number of a card, it returns the name of the card. 8, 9 and 10 represents jack (sota), horse (cavall),
     * king (rei) respectively.
     *
     * @param card between 1 and 10
     * @return name of the given card.
     */
    public static String getCardText(int card) {

        switch (card) {
            case 8:
                System.out.println("jack");
            case 9:
                System.out.println("horse");
            case 10:
                System.out.println("king");
            default:
                return card + "";
        }

    }

    /**
     * Given the number of a card, it returns the value in a 7iMig game. 8, 9 and 10 represents jack, horse,
     * king respectively.
     *
     * @param card between 1 and 10
     * @return value of the gien card.
     */
    public static double getCardValue(int card) {

        switch (card) {
            case 8:
                return 0.5;
            case 9:
                return 0.5;
            case 10:
                return 0.5;
            default:
                return card;
        }
    }

    /**
     * Plays the user turn.
     *
     * @return result of the sum of the user cards value (even if greater than 7.5)
     */
    public static void playUserTurn() {
        Scanner scanner = new Scanner(System.in);
        int randomNumber = ItbRandomizer.nextInt(1, 10);
        double userCounter = getCardValue(randomNumber);
        System.out.println("t'ha sortit la carta " + getCardText(randomNumber));
        System.out.println("Resultat actual: " + userCounter);
        System.out.println("Vols seguir jugant?(0. No || 1. Si");
        int resultQuestion = scanner.nextInt();
        while (resultQuestion == 1) {
            randomNumber = ItbRandomizer.nextInt(1, 10);
            System.out.println("t'ha sortit la carta " + getCardText(randomNumber));
            userCounter = userCounter + getCardValue(randomNumber);
            if (userCounter > 7.5) {
                System.out.println("T'has passat");
                System.exit(0);
            } else {
                System.out.println("t'ha sortit la carta " + userCounter);
                System.out.println("Vols seguir jugant?(0. No || 1. Si");
                resultQuestion = scanner.nextInt();
            }
        }
        System.out.println("El teu resultat és: " + userCounter);
        playComputerTurn(userCounter);


        }

        /**
         * Plays the computer turn.
         * @param userCounter user turn result. The computer will try to obtain at least the same value.
         * @return result of the sum of the computer cards value (even if greater than 7.5)
         */
        private static void playComputerTurn ( double userCounter){
            int computerRandomCard = ItbRandomizer.nextInt(1, 10);
            double computerCounter = getCardValue(computerRandomCard);
            System.out.println("L'ordinaro ha tret " + getCardValue(computerRandomCard));
            System.out.println("Resultat actual de l'ordinado: " + computerCounter);

            while (computerCounter < 7.5 && computerCounter < userCounter) {
                computerRandomCard = ItbRandomizer.nextInt(1, 10);
                System.out.println("L'ordinaro ha tret " + getCardValue(computerRandomCard));
                computerCounter = computerCounter + getCardValue(computerRandomCard);
            }
            if (computerCounter > 7.5) {
                System.out.println("L'ordinador s'ha passat");
                System.out.println("Has guanyat!!!");
            } else if (computerCounter <= 7.5 && computerCounter > userCounter) {
                System.out.println("El resultat de l'ordinador és: " + computerCounter);
                System.out.println("Guanya la banca");
            } else if (computerCounter <= 7.5 && computerCounter == userCounter) {
                System.out.println("El resultat de l'ordinador és: " + computerCounter);
                System.out.println("Empat");
            } else if (computerCounter <= 7.5 && computerCounter < userCounter) {
                System.out.println("El resultat de l'ordinador és: " + computerCounter);
                System.out.println("Has guanyat!!!");
            }

        }
    }
