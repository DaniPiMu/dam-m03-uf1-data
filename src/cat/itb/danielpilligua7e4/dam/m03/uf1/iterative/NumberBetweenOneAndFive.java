package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

//Si introdueix un número més gran o més petit, torna-li a demanar l'enter.

import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        System.out.println("Introdueix un numero entre el 1 i el 5");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        do {
            System.out.println("Proba un altre numero");
            value = scanner.nextInt();
        }while (value>5 || value<1);

    }
}
