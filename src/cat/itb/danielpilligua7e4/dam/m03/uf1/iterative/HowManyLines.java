package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

//Volem contar quantes línies té un text introduït per l'usuari.

import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {
        //l'usuari introdueix un text
        System.out.println("Introduce tu texto");
        Scanner scanner = new Scanner(System.in);
        String txt = scanner.nextLine();
        int i = 0;
        while (!txt.equals ("END")){
            txt = scanner.nextLine();
            i++;
        }
        System.out.print(i);
    }
}
