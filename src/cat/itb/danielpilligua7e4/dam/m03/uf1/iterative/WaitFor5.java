package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

//Farem un programa que preguna a l'usuari un enter fins a que entri el numero 5.

import java.util.Scanner;

public class WaitFor5 {
    public static void main(String[] args) {
        //l'usuari introdueix un enter
        System.out.println("Introduce un numero");
        Scanner scanner = new Scanner(System.in);
        int value1 = scanner.nextInt();
        while (value1 !=5){
            System.out.println("Introdueix un altre cop");
            value1 = scanner.nextInt();
        }
        System.out.println("5 trobat!");
    }
}
