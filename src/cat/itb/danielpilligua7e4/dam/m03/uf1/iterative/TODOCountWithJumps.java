package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class TODOCountWithJumps {
    public static void main(String[] args) {

        //usuario introduce dos enteros
        System.out.println("introduce el valor final y el salto");
        Scanner scanner = new Scanner(System.in);
        int valorFinal = scanner.nextInt();
        int jump = scanner.nextInt();
        int number = 1;
        for (int i = 1; i <= valorFinal;i++){
            if (i==number){
                System.out.print(number + " ");
                number+=jump;
            }
        }
    }
}