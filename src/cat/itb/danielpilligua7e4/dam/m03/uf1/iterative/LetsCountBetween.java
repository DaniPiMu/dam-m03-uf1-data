package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

//Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major

import java.util.Scanner;

public class LetsCountBetween {
    public static void main(String[] args) {
        //usuari introdueix dos valors enters
        System.out.println("introdueix dos valors");
        Scanner scanner = new Scanner(System.in);
        int value1= scanner.nextInt();
        int value2= scanner.nextInt();
        for (int i=value1+1;i<value2;i++){
            System.out.print(i);
        }
    }
}
