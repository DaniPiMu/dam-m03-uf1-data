package cat.itb.danielpilligua7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class CountDown {
    public static void main(String[] args) {
        //usuari introdueix un enter
        System.out.println("introdueix un numero");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        for(int i=value;i!=0;i--){
            System.out.print(i);
        }
    }
}
