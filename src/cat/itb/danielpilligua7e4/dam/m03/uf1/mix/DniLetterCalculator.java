package cat.itb.danielpilligua7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class DniLetterCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int dniUser= scanner.nextInt();
        String[]list=new String[]{"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};

        int residuo= dniUser%23;
        String poscArray= list[residuo];

        System.out.println(dniUser+poscArray);

    }
}
