package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class HowBigIsMyPizza {
    public static void main(String[] args) {
        // preguntamos diametro
        Scanner scanner = new Scanner(System.in);
        double diametro = scanner.nextDouble();

        // calcular superficie
        double result = superficie(diametro);


        // Result

        System.out.println(result);

    }
    private static double superficie(double diametro) {
        return (Math.PI * (diametro/2) * (diametro/2));
    }
}
