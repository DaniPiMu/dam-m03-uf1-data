package com.company;

import java.util.Scanner;

public class IntSum {
    public static void main(String[] args) {
        // preguntamos por un número
        Scanner scanner = new Scanner(System.in);

        // Ponemos 2 numeros y se hace la suma

        int userInputValue1 = scanner.nextInt();
        int userInputValue2 = scanner.nextInt();
        int result = userInputValue1 + userInputValue2;

        // Resultado
        System.out.println(result);

    }
}
