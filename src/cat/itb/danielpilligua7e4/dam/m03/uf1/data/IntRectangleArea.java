package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntRectangleArea {
    public static void main(String[] args) {

        // preguntamos por un número

        Scanner scanner = new Scanner(System.in);
        int base = scanner.nextInt();
        int altura = scanner.nextInt();

        // multiplicamos el valor de la base y el valor de la altura
        int result = rectangleArea(base, altura);

        // result

        System.out.println(result);

    }

    private static int rectangleArea(int base, int altura) {
        return base*altura;
    }
}
