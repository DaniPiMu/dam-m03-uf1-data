package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

//L'usuari introdueix dos enters
// Printa la divisió (entera) i el mòdul dels dos enters en el següent format

import java.util.Scanner;

public class NiceDivide {
    public static void main(String[] args) {
         //preguntamos los 2
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();
        int valor2 = scanner.nextInt();

        // se hace la division
        int division = valor1 / valor2;
        int residuo = valor1 % valor2;

        // result
        System.out.println(valor1 + " dividit entre " + valor2 + " es " + division + " modul " + residuo);




    }

}
