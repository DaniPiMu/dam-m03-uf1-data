package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

// L'usuari introdueix els quilometres recorreguts i el temps minuts que ha tardat en fer el recorregut
//Printa per pantalla la velocitat mitjana en km/hora

import java.util.Scanner;

public class AverageSpeed {
    public static void main(String[] args) {
        // preguntamos kilometros y tiempo
        Scanner scanner = new Scanner(System.in);
        double kilometros = scanner.nextInt();
        double minutos = scanner.nextInt();

        // Calculamos la velocidad media
        double horas = minutos / 60;
        double Result = kilometros / horas;

        // result
        System.out.println(Result);

    }
}
