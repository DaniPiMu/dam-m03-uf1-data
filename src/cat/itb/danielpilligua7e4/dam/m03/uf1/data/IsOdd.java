package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

// Printa la true si és senar, false en qualsevol altre cas.

import java.util.Scanner;

public class IsOdd {
    public static void main(String[] args) {
        // preguntamos numero
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();

        // tenemos que saber si es impar o no
        boolean result = (valor1 % 2 != 0);

        //resultado

        System.out.println(result);


    }
    }

