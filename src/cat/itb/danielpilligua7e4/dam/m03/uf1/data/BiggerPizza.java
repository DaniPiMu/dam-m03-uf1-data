package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

//Queremos saber que pizza es mas grande

import java.util.Scanner;

public class BiggerPizza {

    public static void main(String[] args) {

        // el usuario mete el diametro y los lados de una pizza
        Scanner scanner = new Scanner(System.in);
        double diametro = scanner.nextDouble();
        double base = scanner.nextDouble();
        double altura = scanner.nextDouble();

        // Calculamos
        double superficieredondo = Math.PI * (diametro / 2) * (diametro / 2);
        double superficierectangular = base * altura;
        boolean result = superficieredondo > superficierectangular;

        // Result

        System.out.println(result);



    }
}
