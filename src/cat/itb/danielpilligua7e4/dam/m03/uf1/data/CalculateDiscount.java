package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class CalculateDiscount {
    public static void main(String[] args) {

        // preguntamos numeros
        Scanner scanner = new Scanner(System.in);
        double total = scanner.nextDouble();
        double wDiscount= scanner.nextDouble();

        // calculamos el descuento

        double discount = ((total - wDiscount) / total) *100;

        // Result

        System.out.println(discount);


    }
}