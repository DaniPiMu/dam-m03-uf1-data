package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

//L'usuari introdueix 4 enters
//Printa true si algun és 10 o false en qualsevol altre cas

import java.util.Scanner;

public class OneIs10 {
    public static void main(String[] args) {

        // preguntamos los 4 numeros
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();
        int valor2 = scanner.nextInt();
        int valor3 = scanner.nextInt();
        int valor4 = scanner.nextInt();

        // True si alguno es 10, falso en otro caso
        boolean result = valor1 == 10 || valor2 == 10 || valor3 == 10 || valor4 == 10;

        // resutl
        System.out.println(result);

        }


    }

