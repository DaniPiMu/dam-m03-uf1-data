package com.company;

import java.util.Scanner;

public class IntDoubleMe {
    public static void main(String[] args) {

        // preguntamos por un número
        Scanner scanner = new Scanner(System.in);
        int userInputValue = scanner.nextInt();

        // multiplicamos el numero *2
        int result = userInputValue * 2;

        // resultado
        System.out.println(result);
    }
}


