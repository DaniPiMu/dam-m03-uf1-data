package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsLegalAge {

    public static void main(String[] args) {

        // preguntamos la edad
        Scanner scanner = new Scanner(System.in);
        int age = scanner.nextInt();

        // veamos si es mayor de edad

        boolean result = age >= 18;

        // result
        System.out.println(result);

    }
}
