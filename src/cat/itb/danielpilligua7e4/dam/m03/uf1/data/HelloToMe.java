package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import javax.swing.*;
import java.util.Scanner;

public class HelloToMe {

    public static void main(String[] args) {
        // el usuario pone su nombre

        Scanner scanner = new Scanner(System.in);
        String nombre = scanner.nextLine();

        //imprimir
        System.out.println("Bon dia " + nombre);
    }

}
