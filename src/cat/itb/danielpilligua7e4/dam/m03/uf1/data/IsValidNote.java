package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsValidNote {

    public static void main(String[] args) {

        //preguntamos valor
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();
        // verificacion de que los numero que tiene billete de €

        boolean note5 = valor1 == 5;
        boolean note10 = valor1 == 10;
        boolean note20 = valor1 == 20;
        boolean note50 = valor1 == 50;
        boolean note100 = valor1 == 100;
        boolean note200 = valor1 == 200;
        boolean note500 = valor1 == 500;

        boolean IsValidNote = note5 || note10 || note20 || note50 || note100 || note200 || note500;

        //Result
        System.out.println(IsValidNote);

    }

}
