package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

//L'usuari escriu dos valors i s'imprimeix true si el primer és més gran que el segon i false en qualsevol altre cas.

import java.util.Scanner;

public class IsGreater {
    public static void main(String[] args) {

        //Se piden 2 valores
        Scanner scanner = new Scanner(System.in);
        int userInputValue1 = scanner.nextInt();
        int userInputValue2 = scanner.nextInt();

        //vemos cual es mas grande que el otro
        boolean result = userInputValue1 > userInputValue2;

        //Result

        System.out.println(result);
    }


}
