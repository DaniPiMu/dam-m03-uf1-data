package cat.itb.danielpilligua7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsTeenager {
    public static void main(String[] args) {

        // pregunta edad
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();

        //se comprueba si esta en el rango de edad
        boolean edad = valor1 > 10 && valor1 < 20;

        //result
        System.out.println(edad);

    }


}
