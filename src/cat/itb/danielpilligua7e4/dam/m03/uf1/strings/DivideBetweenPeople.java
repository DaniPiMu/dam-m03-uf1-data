package cat.itb.danielpilligua7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class DivideBetweenPeople {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double preu = scanner.nextInt();
        double j=1;
        for (int i=0; i<10; ++i){
            double preuFinal = preu/j;
            System.out.printf("Dividit entre %.0f persones: %.2f€\n",j,preuFinal);
            ++j;
        }
    }
}
