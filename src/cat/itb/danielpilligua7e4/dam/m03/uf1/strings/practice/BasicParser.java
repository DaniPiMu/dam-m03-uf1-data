package cat.itb.danielpilligua7e4.dam.m03.uf1.strings.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BasicParser{
    public static void main(String[] args) {
        List<String>txt=new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String msg = scanner.nextLine();
        while(!msg.equals("END")){
            txt.add(msg);
            msg = scanner.nextLine();
        }
        for (String s : txt) {
            System.out.printf("%s \n", s.replace("**","\\u001B[1m")
                                        .replace("--","\\u001B[3m")
                                        .replace("++","\\u001B[0m")
                                        .replace("//r//","\\u001B[31m")
                                        .replace("//g//","\\u001B[32m" )
                                        .replace("//b//","\\u001B[34m")
                                        .replace("//s//","\\u001B[39m"));
        }
    }
}
