package cat.itb.danielpilligua7e4.dam.m03.uf1.strings;


/*
L'usuari introduirà una paraula. Imprimeix només les lletres en posició senar:
input: paracaigudista
output: prciuit
 */

import java.util.Scanner;

public class OneYesOneNo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();
        boolean bottom;
        bottom = true;
        for (int i = 0;i<userInput.length();i++){
            if (bottom){
            char parell = userInput.charAt(i);
                System.out.print(parell);
            }
            bottom = !bottom;

        }
    }
}
