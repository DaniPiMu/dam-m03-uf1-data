package cat.itb.danielpilligua7e4.dam.m03.uf1.strings.practice;

import java.util.Scanner;

/*
Implementar un programa que donat el número del mòdul, el número de la unitat formativa i el correu electrònic de l'ITB d'un alumne,
ens calcule quin serà el nom del repositori de Git.

El número del mòdul ha de tindre 2 xifres sempre, omplint amb 0's a l'esquerre si escau.
 */
public class RepositoryName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numeroModul = scanner.nextInt();
        int numeroUF = scanner.nextInt();
        String email = scanner.next();
        String username = email.split("@")[0];

        System.out.printf("%s-dam-m%02d-uf%d",username.replaceAll("\\.",""),numeroModul,numeroUF);
    }
}
