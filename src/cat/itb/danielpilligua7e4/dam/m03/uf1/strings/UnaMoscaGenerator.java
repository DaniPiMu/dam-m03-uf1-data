package cat.itb.danielpilligua7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class UnaMoscaGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String song = "una mosca volava per la llum.\n" +
                "i la llum es va apagar.\n" +
                "i la pobra mosca\n" +
                "es va quedar a les fosques\n" +
                "i la pobra mosca no va poder volar.";
        String userInput = scanner.next();

        System.out.println(song.replaceAll( "[aeiou]", userInput));

        /*char userInput = scanner.next().charAt(0);
        System.out.println(song.replace('a',userInput).replace('e',userInput)
                .replace('i',userInput).replace('o',userInput).
                        replace('u',userInput));*/



        }
    }

