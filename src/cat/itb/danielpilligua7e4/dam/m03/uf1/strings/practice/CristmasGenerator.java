package cat.itb.danielpilligua7e4.dam.m03.uf1.strings.practice;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CristmasGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el año original y el año por el que lo cambiaremos");
        String ogDate= scanner.nextLine();
        String newDate= scanner.nextLine();
        System.out.println("Introduce el nombre original y el nombre por el que lo cambiaremos");
        String ogName = scanner.nextLine();
        String newName = scanner.nextLine();
        System.out.println("Introduce el nombre original del autor y el nombre por el que lo cambiaremos");
        String ogAuthorName = scanner.nextLine();
        String newAuthorName = scanner.nextLine();

        List <String> txt = new ArrayList<>();
        String msg = scanner.nextLine();
         while(!msg.equals("END")){
             txt.add(msg);
             msg = scanner.nextLine();
         }
        for (int i = 0; 1<txt.size();i++) {
            System.out.printf("%s\n",(txt.get(i).replace(ogDate,newDate).replace(ogName,newName).replace(ogAuthorName,newAuthorName)));
        }
        }
    }
