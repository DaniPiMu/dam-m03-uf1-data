package cat.itb.danielpilligua7e4.dam.m03.uf1.staticfunctions;

//Volem fer el joc de pedra paper tisora per jugar contra l'ordinador.
//L'usuari introdueix un enter (1)pedra, (2) paper, (3) tisora.
//L'ordinador decideix aleatoriament la seva tirada.
//Imprimeix per pantalla que tira l'orinador (L'ordinador a tirat pedra).
//Imprimeix per pantalla Has guanyat, Guanya l'ordinador, O empat segons el resultat.

import java.util.Scanner;

public class RockPaperScissorsVsComputer {
    public static void main(String[] args) {
        // el usuario introduce 1 enter
        System.out.println("introduce un numero del 1 al 3");
        System.out.println("1 es piedra");
        System.out.println("2 es paper");
        System.out.println("3 es tisora");
        Scanner scanner = new Scanner(System.in);
        int userValue = scanner.nextInt();
        int randomValue = (int)Math.random()*3+1;

        //Imprimimos lo que ha impreso el ordenador
        switch (randomValue){
            case 1:
                System.out.println("L'ordinador a triat pedra");
                break;
            case 2:
                System.out.println("L'ordinador a triat paper");
                break;
            case 3:
                System.out.println("L'ordinador a triat tisora");
            default:
                System.out.println("error");
        }

        //declarar ganadores
        if (userValue == randomValue){
            System.out.println("empate");
        }
        else if (userValue == 1 && randomValue == 3 || userValue == 2 && randomValue == 1 || userValue == 3 && randomValue == 2) {
            System.out.println("Guanya el primer");
        }
        else if (randomValue == 1 && userValue == 3 || randomValue == 2 && userValue == 1 || randomValue == 3 && userValue == 2){
            System.out.println("Guanya l'ordinador");
        }




    }
}
