package cat.itb.danielpilligua7e4.dam.m03.uf1.staticfunctions;

//demana dos enters (a i b) a l'usuari i imprimeix el valor de aba^bab

import java.util.Scanner;

public class PowerOf {
    public static void main(String[] args) {
    //demanen dos enters
        Scanner scanner = new Scanner(System.in);
        int value1 = scanner.nextInt();
        int value2 = scanner.nextInt();

    //operacion
        double result = Math.pow(value1, value2);

    // result
        System.out.println(result);

    }
}
