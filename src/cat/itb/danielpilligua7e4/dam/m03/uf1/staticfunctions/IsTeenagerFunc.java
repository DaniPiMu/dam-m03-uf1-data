package cat.itb.danielpilligua7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IsTeenagerFunc {
    public static void main(String[] args) {

        // pregunta edad
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();

        //se comprueba si esta en el rango de edad
        boolean edad = isTeenager(valor1);

        //result
        System.out.println(edad);
        
    }

    private static boolean isTeenager(int valor1) {
        return valor1 > 10 && valor1 < 20;

    }
}