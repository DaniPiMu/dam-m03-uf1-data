package cat.itb.danielpilligua7e4.dam.m03.uf1.staticfunctions;

// L'oridador pensa un número del 1 al 3.
//L'usuari introdueix un enter.
//Imprimeix L'has encertat, o No l'has encertat segons el resultat.

import java.util.Scanner;

public class HiddenNumber {
    public static void main(String[] args) {
        // el usuario introduce un numero
        Scanner scanner = new Scanner(System.in);
        int valueInput = scanner.nextInt();

        //numero random 1-3
        double randomValue = Math.random()*3+1;

        //Ha acertado o no
        if (valueInput == randomValue){
            System.out.println("L'has encertat");
        } else {
            System.out.println("No l'has encertat");
        }
    }
}
