package cat.itb.danielpilligua7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PassaLlista {
    public static void main(String[] args) {
        List <String> alumnos = new ArrayList<String>(Arrays.asList("Magalí", "Magdalena", "Magí", "Manel", "Manela", "Manuel", "Manuela", "Mar", "Marc", "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina"));
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();

        while(userInput != -1){
            alumnos.remove (userInput);
            userInput = scanner.nextInt();
        }
        System.out.println(alumnos);
    }
}
