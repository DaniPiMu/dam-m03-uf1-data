package cat.itb.danielpilligua7e4.dam.m03.uf1.arraysdin;

//L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat.
// Afegeix el primer a l'inici de la llista, el segon al final, el tercer a l'inici, etc.

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        boolean addNext = true;

        while (userInput !=-1){
            if (addNext )
                list.add(0, userInput);
            else
                list.add(userInput);

            // preparacion para el siguiente bucle
            addNext =!addNext;
            userInput = scanner.nextInt();
        }
        System.out.println(list);
    }
}