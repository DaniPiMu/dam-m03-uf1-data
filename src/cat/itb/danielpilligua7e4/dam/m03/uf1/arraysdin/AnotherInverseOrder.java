package cat.itb.danielpilligua7e4.dam.m03.uf1.arraysdin;

//L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat.
//Imprimeix-los en l'odre invers al que els ha entrat.

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnotherInverseOrder {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();

        while (userInput !=-1){
            list.add(0, userInput);
            userInput = scanner.nextInt();
        }
            System.out.println(list);
    }
}

