package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//L'usuari escriu un valor que representa una nota
//Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons el la nota numèrica introduïda

import java.util.Scanner;

public class ExamGrade {
    public static void main(String[] args) {

        //Preguntamos la nota
        Scanner scanner = new Scanner(System.in);
        System.out.println("introduce el numero la nota");
        double note = scanner.nextDouble();

        //Result
        if (note > 5) {
            System.out.println("suspendido");
        }
        else if (note > 5 && note < 6){
            System.out.println("suficiente");
        }
        else if (note > 6 && note < 8) {
            System.out.println("be");
        }
        else if (note > 8 && note < 9){
            System.out.println("notable");
        }
        else if (note > 9 && note < 10){
            System.out.println("excelente");
        }
    }
}
