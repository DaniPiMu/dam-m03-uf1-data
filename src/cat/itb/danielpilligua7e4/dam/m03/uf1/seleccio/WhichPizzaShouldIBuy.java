package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//Volem comparar quina pizza és més gran, entre una rectangular i una rodona
//L'usuai entra el diametre d'una pizza rodona
//L'usuari entra els dos costats de la pizza rectangular
//Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compta la rectangular" en qualsevol altre cas.

import java.util.Scanner;

public class WhichPizzaShouldIBuy {
    public static void main(String[] args) {
        // el usuario mete el diametro y los lados de una pizza

        Scanner scanner = new Scanner(System.in);
        System.out.println("introduce diametro");
        double diametro = scanner.nextDouble();
        System.out.println("introduce base y altura");
        double base = scanner.nextDouble();
        double altura = scanner.nextDouble();

        // Calculamos
        double superficieredondo = Math.PI * (diametro / 2) * (diametro / 2);
        double superficierectangular = base * altura;
        boolean result = superficieredondo > superficierectangular;

        //resutl
        if (result){
            System.out.println("compra la rodona");
        }
        else {
            System.out.println("compra la rectangular");
        }
    }
}
