package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//Introdueix el número de persones i el número de galetes.
//Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!", sinó imprimeix "Let's Fight"

import java.util.Scanner;

public class WillWeFightForThecookies {
    public static void main(String[] args) {

        //introducimo numero de personas y numero de galletas
        Scanner scanner = new Scanner(System.in);
        int numeroPersonas = scanner.nextInt();
        int numeroGalletas = scanner.nextInt();

        // Vemos si son iguales
        boolean division = numeroGalletas % numeroPersonas == 0;
        if (division) {
            System.out.println("Let's Eat!");
        }
        else {
            System.out.println("Let's Fight!");

        }

    }

}
