package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

// Demana dos enters per pantalla i imprimex el valor mes gran

import java.util.Scanner;

public class WhichBigger {
    public static void main(String[] args) {

        //Demanen 2 enters
        Scanner scanner = new Scanner(System.in);

        int valor1 = scanner.nextInt();
        int valor2 = scanner.nextInt();

        //valor mes gran
        if (valor1 > valor2) {
            System.out.println(valor1);
        }
        else {
            System.out.println(valor2);
        }
        }

    }



