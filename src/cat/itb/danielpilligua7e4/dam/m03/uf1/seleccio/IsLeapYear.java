package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" o "2021 no és any de traspàs".

import java.util.Scanner;

public class IsLeapYear {
    public static void main(String[] args) {

        //preguntamos el año
        Scanner scanner = new Scanner(System.in);
        int año = scanner.nextInt();

        //resolvemos

        if (año % 4 == 0 && año % 100 != 0) {

            System.out.println(año + "es un año bisiesto");
        } else if (año % 100 == 0 && año % 400 == 0) {

            System.out.println(año + " es un año bisiesto");
        } else
            System.out.println(año + " no es un año bisiesto");




    }
}