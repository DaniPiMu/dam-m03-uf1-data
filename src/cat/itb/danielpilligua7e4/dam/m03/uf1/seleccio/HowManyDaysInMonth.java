package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//Demanar un enter a l'usuari que indica el numero de més
//Retorna el número de dies del mes.

import java.util.Scanner;

public class HowManyDaysInMonth {
    public static void main(String[] args) {
        // pedimos numero del mes
        System.out.println("Dime un numero y te dire cuantos dias tiene");
        Scanner scanner = new Scanner(System.in);
        int mes = scanner.nextInt();
        int days = 0;
        //calculamos los dias del mes
        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                days = 31;
                break;
            case 2:
                days = 28;
                break;
            default:
                days = 30;
        }
        System.out.println("Tiene " +days +" dias");

    }
}
