package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//L'usuari escriu un enter
//imprimeix bitllet vàlid si existeix un bitllet d'euros amb la quantitat entrada, bitllet invàlid en qualsevol altre cas.

import java.util.Scanner;

public class NiceIsValidNote {
    public static void main(String[] args) {

        // preguntamos el numero del billete.
        Scanner scanner = new Scanner(System.in);
        System.out.println("introduce el numero del billete");
        int note = scanner.nextInt();

        //comprobamos que el numero exista en formato billete
        boolean note5 = note == 5;
        boolean note10 = note == 10;
        boolean note20 = note == 20;
        boolean note50 = note == 50;
        boolean note100 = note == 100;
        boolean note200 = note == 200;
        boolean note500 = note == 500;
        boolean validNote = note5 || note10 || note20 || note50 || note100 || note200 || note500;

        // conditions
        if (validNote) {
            System.out.println("billet valid");
        }
        else {
            System.out.println("billet no valid");
        }


        }

    }
