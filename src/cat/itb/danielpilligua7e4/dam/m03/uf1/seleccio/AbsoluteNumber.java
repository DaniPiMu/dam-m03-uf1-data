package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//Printa el valor absolut d'un enter entrat per l'usuari.

import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {

        //preguntamos el numero
        Scanner scanner = new Scanner(System.in);
        int valor = scanner.nextInt();
        int falso = valor + (-valor * 2);
        //vemos el valor absoluto
        if (valor < 0){
            System.out.println(falso);
        }
        else {
            System.out.println(valor);
        }
    }

}
