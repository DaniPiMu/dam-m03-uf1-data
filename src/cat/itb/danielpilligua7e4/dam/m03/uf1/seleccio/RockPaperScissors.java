package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//Volem fer el joc de pedra paper tisora.
//L'usuari introdueix dos enters (1)pedra, (2) paper, (3) tisora.
//Imprimeix per pantall Guanya el primer, Guanya el segon, Empat segons els enters introduits.

import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        //preguntamos los 2 valores
        System.out.println("introduce dos numero del 1 al 3");
        System.out.println("1 es piedra");
        System.out.println("2 es paper");
        System.out.println("3 es tisora");
        Scanner scanner = new Scanner(System.in);
        int userValue = scanner.nextInt();
        int userValue2 = scanner.nextInt();

        //Vemos quien gana
        if (userValue == userValue2) {
            System.out.println("empate");
        }
        else if (userValue == 1 && userValue2 == 3 || userValue == 2 && userValue2 == 1 || userValue == 3 && userValue2 == 2)
        System.out.println("Guanya el primer");
        else if (userValue2 == 1 && userValue == 3 || userValue2 == 2 && userValue == 1 || userValue2 == 3 && userValue == 2)
            System.out.println("Guanya el segon");
    }
}
