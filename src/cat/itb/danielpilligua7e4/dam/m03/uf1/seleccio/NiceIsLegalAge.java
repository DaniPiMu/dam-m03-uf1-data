package cat.itb.danielpilligua7e4.dam.m03.uf1.seleccio;

//demani l'edat de l'usuari
//printa ets major d'edat si és major d'edat.

import java.util.Scanner;

public class NiceIsLegalAge {
    public static void main(String[] args) {

        //preguntamos la edad del usuario
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();

        //Resolvemos
        if (valor1 > 18) {
            System.out.println("ets major d'edat");
        }
        else {
            System.out.println("no ets major d'edat");

        }
    }

}
