package cat.itb.danielpilligua7e4.dam.m03.uf1.generalexam;

import java.util.Scanner;

public class GenderGuesser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();
        while (!userInput.equals("END")) {
            if (userInput.endsWith("a")) {
                System.out.println("femení");
            } else if (userInput.endsWith("s")) {
                System.out.println("plural");
            } else {
                System.out.println("masculí");
            }

            }
        }

    }
    //me hace sout, pero uno por uno. No puedo poner una lista y que me los imprima despues a la vez
