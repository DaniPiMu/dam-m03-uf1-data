package cat.itb.danielpilligua7e4.dam.m03.uf1.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//L'empresa de rellotges esportius RunWithMe ens ha demanat que fem un petit
// mòdul per els seus rellotges intel·ligents per el control del rítme cardíac.
// Volem tenir controlat quan un esportista té el ritme cardíac massa alt o
// massa baix durant un període de temps elevat de forma consecutiva.
public class HeartRateWarning {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int minim = scanner.nextInt();
        int maxim = scanner.nextInt();
        int ritmos = scanner.nextInt();
        List<Integer> diferentesRitmos = new ArrayList<Integer>();
        while (ritmos != -1) {
            diferentesRitmos.add(ritmos);
            ritmos = scanner.nextInt();
        }
        int contadorMn = 0;
        int contadorMx = 0;
        for (Integer diferentesRitmo : diferentesRitmos) {
            if (diferentesRitmo < minim) {
                contadorMn++;
            }
            minim = scanner.nextInt();
        for (Integer diferentesRitmo1 : diferentesRitmos) {
            if (diferentesRitmo1 > maxim) {
                contadorMx++;
                }
            maxim = scanner.nextInt();
            }
        }

        System.out.print(contadorMn);
        System.out.print(contadorMx);
    }

}
// queria hacer un contador para cada uno, y de ahi si se pudiese dividir entre 3, que printara. Se que no pide eso, pero de ahi veria que sale.