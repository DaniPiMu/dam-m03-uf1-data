package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        int [] array = new int[10];
        Scanner scanner = new Scanner(System.in);

        int userInput = scanner.nextInt();
        int userInput2= scanner.nextInt();

        while (userInput != -1||userInput2!=-1){
            array[userInput] = userInput2;
            userInput = scanner.nextInt();
            userInput2= scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
    }
}
// revisar los scanners el porque hay que poner dos -1