package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//Esta funcion nos permite preguntar al usuario el tamaño de la Array y ademas, que el mismo la complete.

import java.util.Scanner;

public class ArrayReader {

    public static int[] scannerReadIntArray(Scanner scanner){
        int size = scanner.nextInt();
        int[] values = new int[size];

        for(int i=0; i<size; ++i){
            values[i] = scanner.nextInt();
        }
        return values;
    }

}
