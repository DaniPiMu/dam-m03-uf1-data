package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres), tenint en compte que dilluns és el 0.

import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        System.out.println("Introduce un numero y te digo el dia de la semana");
        String[] days = {"dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge" };
        Scanner scanner = new Scanner(System.in);
        int daynumber = scanner.nextInt();
        String daysResult = days[daynumber];
        System.out.println(daysResult);
    }
}
