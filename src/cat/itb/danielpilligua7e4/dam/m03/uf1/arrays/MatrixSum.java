package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;
//Imrimeix la suma de tots els seus valors.
public class MatrixSum {
    public static void main(String[] args) {
        int[][] matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}};
        int suma=0;
        for (int i=0; i< matrix.length;++i){
            for (int j=0;j< matrix[i].length;++j){
                int numeroActual= matrix [i][j];
                suma = suma+numeroActual;
            }
        }
        System.out.println(suma);
    }
}
