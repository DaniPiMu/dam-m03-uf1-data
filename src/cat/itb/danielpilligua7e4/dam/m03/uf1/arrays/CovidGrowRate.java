package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CovidGrowRate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] values = new int[size];

        for (int i = 0; i < size; ++i) {
            values[i] = scanner.nextInt();
        }
        for (int i = 0; i < values.length; i++) {
            double current = values[i];
            double previous = values[i-1];
            double rate = current/previous;
            // rates[i-1] = rate;
            System.out.println(rate);
        }
    }
}

