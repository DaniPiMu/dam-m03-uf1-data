package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//L'usuari entra 10 enters. Imprimeix-los en l'odre invers al que els ha entrat.

import java.util.Scanner;

public class InverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = new int[10];
        for (int i = 0; i < 10; i++) {
            int value = scanner.nextInt();
            values[i] = value;
        }
        for (int i = 9; i >= 0; --i) {
            int value = values[i];
            System.out.println(value);


        }
    }
}
