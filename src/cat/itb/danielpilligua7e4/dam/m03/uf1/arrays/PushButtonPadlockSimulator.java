package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//Candau amb 8 botons, el primer será el 0.
// - l'usuari introduira enters indicant quin bto ha d'apretar
// - Quan introduiexi el -1, es que ja ha acabat i hem d'imorimir l'estat del candau

import java.util.Arrays;
import java.util.Scanner;

public class PushButtonPadlockSimulator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean [] botton = new boolean[7];
        int userNumber = scanner.nextInt();

        //mientras el usuario no ponga -1
            //pido posicion a l'usuario
            //modifico la posicion introducida del array
        while ( userNumber!= -1) {
            botton[userNumber] = !botton[userNumber];
            userNumber = scanner.nextInt();
        }
        System.out.println(Arrays.toString(botton));
    }
}
