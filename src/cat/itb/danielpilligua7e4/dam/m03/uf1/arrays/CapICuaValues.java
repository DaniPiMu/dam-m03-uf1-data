package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//Printa per pantalla cap i cua si la llista de N valors introduits per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).

import java.util.Scanner;

public class CapICuaValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] values = new int[size];

        for (int i=0; i< size; i++){
            values[i] = scanner.nextInt();
        }
        boolean result = true;
        for (int i = 0; i < values.length/2; ++i){
            int cap = values[i];
            int cua= values[values.length -1 -i];
            if (cap != cua){
                result = false;
                break;
            }
        }
        System.out.println(result);
    }
}
// comparar la array que introducio el usuario, con la misma array pero invertida
// si es igual, capicua
