package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//L'usuari entra 10 enters. Crea un array amb aquest valors.
//Imprimeix per pantalla el valor més petit introduit.

import java.util.Scanner;

public class MinOf10Values {
    public static void main(String[] args) {
        int[] values = new int[10];

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i<10 ; i++ ) {
            int value = scanner.nextInt();
            values[i] = value;
        }
        int minValue = values[0];
        minValue = Math.min(minValue,values[1]);
        System.out.println(minValue);
    }
}
