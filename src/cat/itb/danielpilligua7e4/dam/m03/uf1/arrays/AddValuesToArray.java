package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//Inicialitza un array de floats de tamany 50, amb el valor 0.0f a tots els elements.
//Després asigna els els valors següents a les posicions indicades:

import java.util.Arrays;

public class AddValuesToArray {
    public static void main(String[] args) {
    float [] array = new float[50];
    array [0] = 31.0f;
    array [1] = 56.0f;
    array [19] = 12.0f;
    array [49] = 79.0f;
        System.out.println(Arrays.toString(array));
    }
}
