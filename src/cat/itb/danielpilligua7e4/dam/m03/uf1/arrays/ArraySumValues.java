package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//L'usuari introduix una llista de valors tal i com s'indica al mètode ArrayReader.
//Imprimeix per pantalla la suma d'aquests valors.

import java.util.Scanner;

public class ArraySumValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Con esta funcion, preguntamos el tamaño de la array y la completa.
        int[] values = ArrayReader.scannerReadIntArray(scanner);
        int suma=0;
        for (int value:values){
            suma = suma + value;
        }
        System.out.println(suma);

    }
}
