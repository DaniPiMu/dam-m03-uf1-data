package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class BicicleDistance {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double distance = scanner.nextDouble();
        int[] secs = {1,2,3,4,5,6,7,8,9,10};
        int i;
        for(i=0;i<10;++i){
            double result = secs[i]*distance;
            System.out.println(result);
        }
    }
}