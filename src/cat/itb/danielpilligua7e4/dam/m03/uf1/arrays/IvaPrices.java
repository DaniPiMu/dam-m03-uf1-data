package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//En una botiga volem convertir tot de preus sense a IVA al preu amb IVA. Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
//L'usuari introduirà el preu de 10 artícles. Imprimeix per pantalla el preu amb l'IVA afegit amb el següent format:

import java.util.Scanner;

public class IvaPrices {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        double iva = 0.21;
        float[] articles = new float[9];
        for (int i =0; i<9; i++){
            articles[i] = scanner.nextInt();
        }
        for (int i = 0;i<9;i++){
        double percentageIva = articles[i]*iva;
        double result = articles[i]+percentageIva;
            System.out.println(articles[i] + " IVA " + "= " + result);
        }
    }
}
