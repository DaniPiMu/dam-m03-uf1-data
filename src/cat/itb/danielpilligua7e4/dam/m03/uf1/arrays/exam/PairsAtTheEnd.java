package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PairsAtTheEnd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        List<Integer> list = new ArrayList<Integer>();

        int parells = userInput % 2;

        while (userInput != -1) {
            if (parells == 0) {
                list.add(0, userInput);
            } else {
                list.add(userInput);
            }
            userInput = scanner.nextInt();
            parells = userInput % 2;
        }
        System.out.println(list);
    }
}
