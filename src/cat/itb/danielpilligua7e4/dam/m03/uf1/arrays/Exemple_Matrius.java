package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

public class Exemple_Matrius {
    public static void main(String[] args) {
        //creamos matriz
        int [][] table= {{1,3,4}, {1,2,3}, {3,2,4}, {1,2,3}};

        //Seleccionamos lo que hay dentro del array
        int value = table [1][2]; //de la tabla 2 nos cojera el numero en la posicion 3, en este caso el 3

        //modificamos lo que hay dentro de la matriz
        table[1][2]= 5;

        //seleccionamos un "cajon" entero
        int[] row = table [1];
    }
}
