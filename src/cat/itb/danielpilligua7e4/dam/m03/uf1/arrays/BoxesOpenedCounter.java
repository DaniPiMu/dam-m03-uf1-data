package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//L'usuari introduirà enters del 0 al 10 quan s'obri la caixa indicada.
//Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.

import java.util.Arrays;
import java.util.Scanner;

public class BoxesOpenedCounter {
    public static void main(String[] args) {
        int [] boxOpenedCounter = new int [10];
        Scanner scanner = new Scanner(System.in);

        int openBox = scanner.nextInt();
        while (openBox != -1){
            boxOpenedCounter [openBox] = boxOpenedCounter[openBox] + 1;
            openBox = scanner.nextInt();
        }
        System.out.println(Arrays.toString(boxOpenedCounter));
    }
}
