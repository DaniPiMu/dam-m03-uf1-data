package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays.exam;

import java.util.Scanner;

public class SumPositiveValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] values = new int[size];

        for (int i = 0; i < size; ++i) {
            values[i] = scanner.nextInt();
        }
        int suma = 0;
        for (int value : values) {
            if (value >= 0) {
                suma = suma + value;
            } else;
        }
        System.out.println(suma);
    }
}
