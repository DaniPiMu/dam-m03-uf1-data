package cat.itb.danielpilligua7e4.dam.m03.uf1.arrays;

//Printa per pantalla ordenats si la llista de N valors introduits per l'usuari estan ordenats.
//
//L'usuari primer entrarà el número d'enters a introudir i després els diferents enters.

import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userValue = scanner.nextInt();
        int [] array1 = new int [userValue];
        for (int i =0; i< userValue;i++) {
            userValue = scanner.nextInt();
            array1[i] = userValue;
        }
        boolean sorted = true;
        for (int i =0; 1+i < array1.length; i++){
            int value = array1[i];
            int nextValue = array1[1+i];

            if (value<nextValue){
                sorted = false;
                break;
            }
        }
        if(sorted)
            System.out.println("ordenat");

    }
}

